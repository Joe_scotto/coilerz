<?php

class Database {
	private static $_instance;
	public $pdo;

	/**
	 * Singleton for database connection
	 * @return class Returns one instance of the Database class
	 */
	public static function getInstance() {
		if(!self::$_instance) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	/**
	 * Connects to database
	 * @return mysql PDO connection to mysql databse
	 */
	public function getConnection() {
		try {
			//Attempt to connect
			$this->pdo = new PDO('mysql:dbname=' . Config::get('mysql/db'), Config::get('mysql/user'), Config::get('mysql/pass'));

			//Return connection
			return $this->pdo;
		} catch (PDOException $e) {
			throw new PDOException($e);
		}
	}

	/**
	 * @param  string $string What is getting hashed
	 * @return string Returns a hashed string
	 */
	public function hashPassword ($string) {
		$hash = password_hash($string, PASSWORD_BCRYPT, array(
			'cost' => 10
		));
		
		return $hash;
	}

	/**
	 * Decodes base64 string and uploads jpeg to specified directory
	 * @param  string $base64 String supplied by Cropit
	 * @param  mixed $directory Where the image should go once uploaded
	 * @return bool
	 */
	public function decodeBase64 ($base64, $directory) {
	    list($type, $base64) = explode(';', $base64);
	    list(, $base64)      = explode(',', $base64);
	    $base64 = base64_decode($base64);
	    
	    file_put_contents($directory, $base64);
	}

	/**
	 * Checks the dimensions of the provided image
	 * @param  string $base64_image Base64 string of the image
	 * @param  string $width Desired width of the image
	 * @param  string $height Desired height of the image
	 * @return bool True if dimensions match, false if dimensions do not match
	 */
	public function checkImageDimensions ($base64_image, $width, $height) {
		list($type, $base64_image) = explode(';', $base64_image);
	    list(, $base64_image)      = explode(',', $base64_image);
	    $base64_image = base64_decode($base64_image);

	    $dimensions = getimagesizefromstring($base64_image);

	    if ($dimensions[0] == $width && $dimensions[1] == $height) {
	    	return true;
	    } else if ($dimensions[0] !== $width && $dimensions[1] !== $height) {
	 		return false;
		}
	}
}

