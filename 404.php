<?php require 'core/init.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<!-- Bootstrap Mobile Optimization -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<!-- Meta Tags -->
	<title>Coilerz - Welp! We found a short</title>
	<!-- Favicon -->
	<link rel="icon" href="images/logo.png">
	<!-- Main Stylesheet -->
	<link rel="stylesheet" href="css/style.css">
	<!-- Bootstrap CDN CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
</head>
<body>
	<!-- Header -->
	<?php require 'templates/header.php'; ?>

	<!-- Content -->
	<div id="not_found_content">
		<div class="container">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"></div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
	        	<img src="images/alert.png" class="img-responsive text-center">
	    	</div>
	    	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"></div>
	    	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
		        <h1>Whoa!</h1>
		        <h3>It seems you have a short</h3>
		        <h4>This page will vent in: <div id="not_found_countdown">5</div></h4>
		    </div>
		</div>
	</div>

	<!-- Footer -->
	<?php require 'templates/footer.php' ?>

	<script>

		//Redirect Function
     	function startTimer(duration, display) {
            var timer = duration, minutes, seconds;
            setInterval(function () {
                minutes = parseInt(timer / 60, 10);
                seconds = parseInt(timer % 60, 10);
        
                minutes = minutes < 10 ? "" + minutes : minutes;
                seconds = seconds < 10 ? "" + seconds : seconds;
        
                display.textContent = seconds;
        
                if (--timer < 0) {
                    timer = duration;
                }
            }, 1000);
        }
        
        //Run startTimer() on page load
        window.onload = function () {
            var fiveMinutes = 4;
                display = document.querySelector('#not_found_countdown');
            startTimer(fiveMinutes, display);
        };
        
        //Wait 5 seconds
        setTimeout(function (e) {
            window.location.replace("index.php");
        }, 5000); 

    </script>	
</body>
</html>