<?php

class Mail {
	/**
	 * Sends an email to contact@coilerz.com
	 * @param  mixed $data $_POST Data
	 * @param  string $url Link to post back to
	 * @return string Success Message
	 */
	public function sendMessage ($data, $url) {
		//Send Email
		mail("contact@coilerz.com", $data['contact_subject'], "From: " . $data['contact_name'] . "\n"  . "Email: " . $data['contact_email'] . "\n\n" . $data['contact_message'], "From: New Message <contact@coilerz.com>");

		//Redirect to prevent double submission
		setcookie("sessionPersist", 1);
		$_SESSION['message'] = "Your email has been sent, please allow up to 7 days for a response.";
		header("Location: " . $url);
	}

	/**
	 * Flags a coil
	 * @param  mixed $data $_POST Data
	 * @param  string $coil_id ID of the coil getting flagged
	 * @param  string $url URL of the coil
	 * @return string Success Message
	 */
	public static function flagCoil ($data, $coil_id, $url) {
		//Declare mail variables
		$coil = new Coil();
		$subject = "Flag - " . $coil->getCoilInfo($coil_id)['coil_name'];;

		//Send Email
		mail("flag@coilerz.com", $data['coil_flag_reason'],"Coil ID: " . $coil_id . "\n\n" . $url, "From: Flagged Coil <flag@coilerz.com>");

		//Redirect to prevent double submission
		setcookie("sessionPersist", 1);
		$_SESSION['message'] = "Coil was successfully flagged";
		header("Location: " . $url);
	}
}