<?php require 'core/init.php'; ?>
<?php Coil::checkIfCoilsExists($_GET['coil_id']); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<!-- Bootstrap Mobile Optimization -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<!-- Meta Tags -->
	<title>Coilerz - <?php echo htmlspecialchars($coilInfo['coil_name']); ?></title>
	<meta property="og:image" content="<?php echo htmlspecialchars($coilInfo['image']);?>" />
	<meta property="og:description" content="<?php echo htmlspecialchars($coilInfo['coil_name'])?>" />
	<meta property="og:url"content="<?php echo htmlspecialchars($url); ?>" />
	<meta property="og:title" content="Coil build shared from Coilerz" />
	<!-- Favicon -->
	<link rel="icon" href="images/logo.png">
	<!-- Main Stylesheet -->
	<link rel="stylesheet" href="css/style.css">
	<!-- Bootstrap CDN CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
</head>
<body>
	<!-- Header -->
	<?php require 'templates/header.php'; ?>
	
	<!-- Share Modal -->
	<div class="modal fade share_modal" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<div class="modal-header" style="padding:10px 50px;">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4>Share this coil</h4>
				</div>
				<div class="modal-body" style="padding:15px 50px;">
					<div class="row">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
				     	   <img src="images/facebook.png" onclick="popup('http://www.facebook.com/share.php?u=<?php echo htmlentities($url); ?>', 'Share on Facebook');" class="img-responsive">
				     	</div>
				     	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
	        				<img src="images/twitter.png" onclick="popup('http://twitter.com/intent/tweet?status=<?php echo htmlentities($coilInfo['coil_name']) . " at: " . htmlentities($url); ?>', 'Share on Twitter');" class="img-responsive">
	        			</div>
	        			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
	        				<img src="images/googleplus.png" onclick="popup('https://plus.google.com/share?url=<?php echo htmlentities($url); ?>', 'Share on Google+');" class="img-responsive">
	        			</div>
	        		</div>
				</div>
			</div>
		</div>
	</div>

	<?php if (Coil::checkCoilOwner($_SESSION['session_id'], $_GET['coil_id'])) { ?>
	<!-- Delete Modal -->
	<div class="modal fade delete_modal" id="deleteConfirmModal" tabindex="-1" role="dialog" aria-labelledby="deleteLabel" aria-hidden="true">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header">
	            	<button type="button" class="close" data-dismiss="modal">&times;</button>
	                <h4>Are you sure?</h4>
	            </div>
	            <div class="modal-body">
	                <p>You have selected to delete this coil</p>
	                <p>Once a coil is deleted, it is gone from our servers forever. Are you absolutely certain that you want to delete this coil?</p>
	            </div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
	                <form method="post" action="">
	                	<input type="submit" class="btn btn-danger" id="confirm_deletion" name="confirm_deletion" value="Delete Coil">
	                </form>
	            </div>
	        </div>
	    </div>
	</div>
	<?php } ?>

	<?php if (Coil::checkCoilOwner($_SESSION['session_id'], $_GET['coil_id'])) { ?>
	<!-- Edit Modal -->
	<div class="modal fade edit_modal" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<div class="modal-header" style="padding:10px 50px;">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<?php if (isset($_SESSION['edit_error'])) { ?>
					<h4 class="error"><?php echo htmlspecialchars($_SESSION['edit_error']);?></h4>
					<?php } else { ?>
					<h4>Edit Coil</h4>
					<?php }?>
				</div>
				<form action="" method="post" data-toggle="validator" name="edit_form" id="edit_form" enctype="multipart/form-data">
					<div class="modal-body" style="padding:15px 50px;">
						<div class="row">
							<!-- Hidden Identifier -->
							<input type="text" name="edit_form_identifier" id="edit_form_identifier" value="<?php echo htmlspecialchars($_POST['edit_form_identifier']); ?>">
							<!-- Coil Name -->
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<div class="form-group">
									<label for="coil_name">Coil Name*</label>
									<input type="text" class="form-control" id="coil_name" placeholder="Coil Name" name="edit_coil_name" value="<?php echo htmlspecialchars($coilInfo['coil_name']); ?>" maxlength="32" required>
								</div>
							</div>
							<!-- Resistance -->
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<div class="form-group">
									<label for="resistance">Resistance*</label>
									<div class="input-group">
										<input type="number" class="form-control" id="resistance" placeholder="Resistance" name="edit_resistance" step="0.001" min="0" value="<?php echo htmlspecialchars($coilInfo['resistance']); ?>"  required>
										<div class="input-group-addon">Ω</div>
									</div>
								</div>
							</div>
							<!-- Inner Diameter -->
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 multi_selector">
								<div class="col-lg-8 col-md-8 col-sm-10 col-xs-10 main">
									<div class="form-group">
										<label for="inner_diameter">Inner Diameter*</label>
										<input id="inner_diameter" class="form-control" type="number" placeholder="Inner Diameter" name="edit_inner_diameter" step="0.00001" min="0" value="<?php echo htmlspecialchars(preg_split('#(?<=\d)(?=[a-z])#i', $coilInfo['inner_diameter'])[0]); ?>" required>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-2 col-xs-2 secondary">
									<div class="form-group">
										<label for="edit_inner_diameter_unit">&nbsp;</label>
										<div class="dropdown_arrow">&#9662;</div>
										<select name="edit_inner_diameter_unit" id="" class="form-control" id="edit_inner_diameter_unit" required>
											<option value="mm" <?php if (preg_split('#(?<=\d)(?=[a-z])#i', $coilInfo['inner_diameter'])[1] == 'mm') echo htmlentities('selected'); ?>>mm</option>
											<option value="in" <?php if (preg_split('#(?<=\d)(?=[a-z])#i', $coilInfo['inner_diameter'])[1] == 'in') echo htmlentities('selected'); ?>>in</option>
										</select>
									</div>
								</div>
							</div>
							<!-- Wraps -->
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<div class="form-group">
									<label for="wraps">Wraps*</label>
									<input type="number" class="form-control" id="wraps" placeholder="Wraps" name="edit_wraps"  step="1" min="0" value="<?php echo htmlspecialchars($coilInfo['wraps']); ?>" required>
								</div>
							</div>
							<!-- Number of Coils -->
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<div class="form-group">
									<label for="number_of_coils">Number of Coils*</label>
									<input type="number" class="form-control" id="number_of_coils" placeholder="Number of Coils" name="edit_number_of_coils"  step="1" min="1" max="50" value="<?php echo htmlspecialchars($coilInfo['coils']); ?>" required>
								</div>
							</div>
							<!-- Atomizer -->
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<div class="form-group">
									<label for="atomizer">Atomizer</label>
									<input type="text" class="form-control" id="atomizer" placeholder="Atomizer" name="edit_atomizer" value="<?php echo htmlspecialchars($coilInfo['atomizer']); ?>" maxlength="32">
								</div>
							</div>
							<!-- Difficulty -->
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<label for="">Difficulty*</label>
								<div class="btn-group btn-group-justified" data-toggle="buttons">
									<label class="btn btn-primary <?php if (strtolower($coilInfo['difficulty']) == 'easy') echo htmlentities('active'); ?>">
										<input type="radio" name="edit_difficulty" id="easy" value="easy" <?php if (strtolower($coilInfo['difficulty']) == 'easy') echo htmlentities('checked'); ?>>Easy
									</label>
									<label class="btn btn-primary <?php if (strtolower($coilInfo['difficulty']) == 'medium') echo htmlentities('active'); ?>">
										<input type="radio" name="edit_difficulty" id="medium" value="medium" <?php if (strtolower($coilInfo['difficulty']) == 'medium') echo htmlentities('checked'); ?>>Medium
									</label>
									<label class="btn btn-primary <?php if (strtolower($coilInfo['difficulty']) == 'hard') echo htmlentities('active'); ?>">
										<input type="radio" name="edit_difficulty" id="hard" value="hard" <?php if (strtolower($coilInfo['difficulty']) == 'hard') echo htmlentities('checked'); ?>>Hard
									</label>
									<label class="btn btn-primary <?php if (strtolower($coilInfo['difficulty']) == 'experimental') echo htmlentities('active'); ?>">
										<input type="radio" name="edit_difficulty" id="experimental" value="experimental" <?php if (strtolower($coilInfo['difficulty']) == 'experimental') echo htmlentities('checked'); ?>>Experimental
									</label>
								</div>
							</div>
							<!-- Coil Image -->
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="form-group">
									<label for="wraps">Coil Image*</label>
									<label for="cropit-image-input-edit" id="select_image">Select Image</label>
									<hr id="edit_divider">
									<div class="image-editor-edit">
										<input type="file" class="cropit-image-input" id="cropit-image-input-edit" accept="jpg, jpeg, png" name="edit_file">
										<div class="cropit-preview"></div>
										<input type="text" id="hidden_base64_edit" name="edit_image">
										<input type="range" class="cropit-image-zoom-input range">
									</div>
									<hr>
								</div>
							</div>
							<!-- Description -->
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="form-group">
									<label for="edit_description">Description*</label>
									<textarea name="edit_description" class="form-control" id="edit_description" maxlength="512" required><?php echo htmlspecialchars($coilInfo['description']); ?></textarea>														
									<hr>
								</div>
							</div>
							<!-- Wire One -->
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 multi_selector">
								<div class="form-group">
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 main">
										<div class="form-group">
											<label for="wire_one_gauge">Wire One*</label>
											<input id="wire_one_gauge" class="form-control" type="number" placeholder="AWG" name="edit_wire_one_gauge" value="<?php echo htmlspecialchars(explode(",", $coilInfo['wire_gauges'])[0]); ?>" step="1" min="0" max="99" required>
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 secondary">
										<div class="form-group">
											<label for="wire_one_type">&nbsp;</label>
											<div class="dropdown_arrow">&#9662;</div>
											<select name="edit_wire_one_type" id="" class="form-control" required>
												<option value="Kanthal" <?php if (htmlspecialchars(explode(",", $coilInfo['wire_types'])[0]) == 'Kanthal') echo htmlentities('selected'); ?>>Kanthal</option>
												<option value="N200" <?php if (htmlspecialchars(explode(",", $coilInfo['wire_types'])[0]) == 'N200') echo htmlentities('selected'); ?>>N200</option>
												<option value="N80" <?php if (htmlspecialchars(explode(",", $coilInfo['wire_types'])[0]) == 'N80') echo htmlentities('selected'); ?>>N80</option>
												<option value="Stainless Steel" <?php if (htmlspecialchars(explode(",", $coilInfo['wire_types'])[0]) == 'Stainless Steel') echo htmlentities('selected'); ?>>Stainless Steel</option>
												<option value="Titanium" <?php if (htmlspecialchars(explode(",", $coilInfo['wire_types'])[0]) == 'Titanium') echo htmlentities('selected'); ?>>Titanium</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<!-- Wire Two -->
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 multi_selector">
								<div class="form-group">
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 main">
										<div class="form-group">
											<label for="wire_two_gauge">Wire Two</label>
											<input id="wire_two_gauge" class="form-control" type="number" placeholder="AWG" name="edit_wire_two_gauge" value="<?php echo htmlspecialchars(explode(",", $coilInfo['wire_gauges'])[1]); ?>" step="1" min="0" max="99">
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 secondary">
										<div class="form-group">
											<label for="wire_two_type">&nbsp;</label>
											<div class="dropdown_arrow">&#9662;</div>
											<select name="edit_wire_two_type" id="" class="form-control">
												<option value="Kanthal" <?php if (htmlspecialchars(explode(",", $coilInfo['wire_types'])[1]) == 'Kanthal') echo htmlentities('selected'); ?>>Kanthal</option>
												<option value="N200" <?php if (htmlspecialchars(explode(",", $coilInfo['wire_types'])[1]) == 'N200') echo htmlentities('selected'); ?>>N200</option>
												<option value="N80" <?php if (htmlspecialchars(explode(",", $coilInfo['wire_types'])[1]) == 'N80') echo htmlentities('selected'); ?>>N80</option>
												<option value="Stainless Steel" <?php if (htmlspecialchars(explode(",", $coilInfo['wire_types'])[1]) == 'Stainless Steel') echo htmlentities('selected'); ?>>Stainless Steel</option>
												<option value="Titanium" <?php if (htmlspecialchars(explode(",", $coilInfo['wire_types'])[1]) == 'Titanium') echo htmlentities('selected'); ?>>Titanium</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<!-- Wire Three -->
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 multi_selector">
								<div class="form-group">
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 main">
										<div class="form-group">
											<label for="wire_three_gauge">Wire Three</label>
											<input id="wire_three_gauge" class="form-control" type="number" placeholder="AWG" name="edit_wire_three_gauge" value="<?php echo htmlspecialchars(explode(",", $coilInfo['wire_gauges'])[2]); ?>" step="1" min="0" max="99">
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 secondary">
										<div class="form-group">
											<label for="wire_three_type">&nbsp;</label>
											<div class="dropdown_arrow">&#9662;</div>
											<select name="edit_wire_three_type" id="" class="form-control">
												<option value="Kanthal" <?php if (htmlspecialchars(explode(",", $coilInfo['wire_types'])[2]) == 'Kanthal') echo htmlentities('selected'); ?>>Kanthal</option>
												<option value="N200" <?php if (htmlspecialchars(explode(",", $coilInfo['wire_types'])[2]) == 'N200') echo htmlentities('selected'); ?>>N200</option>
												<option value="N80" <?php if (htmlspecialchars(explode(",", $coilInfo['wire_types'])[2]) == 'N80') echo htmlentities('selected'); ?>>N80</option>
												<option value="Stainless Steel" <?php if (htmlspecialchars(explode(",", $coilInfo['wire_types'])[2]) == 'Stainless Steel') echo htmlentities('selected'); ?>>Stainless Steel</option>
												<option value="Titanium" <?php if (htmlspecialchars(explode(",", $coilInfo['wire_types'])[2]) == 'Titanium') echo htmlentities('selected'); ?>>Titanium</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<!-- Wire Four -->
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 multi_selector">
								<div class="form-group">
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 main">
										<div class="form-group">
											<label for="wire_four_gauge">Wire Four</label>
											<input id="wire_four_gauge" class="form-control" type="number" placeholder="AWG" name="edit_wire_four_gauge" value="<?php echo htmlspecialchars(explode(",", $coilInfo['wire_gauges'])[3]); ?>" step="1" min="0" max="99">
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 secondary">
										<div class="form-group">
											<label for="wire_four_type">&nbsp;</label>
											<div class="dropdown_arrow">&#9662;</div>
											<select name="edit_wire_four_type" id="" class="form-control">
												<option value="Kanthal" <?php if (htmlspecialchars(explode(",", $coilInfo['wire_types'])[3]) == 'Kanthal') echo htmlentities('selected'); ?>>Kanthal</option>
												<option value="N200" <?php if (htmlspecialchars(explode(",", $coilInfo['wire_types'])[3]) == 'N200') echo htmlentities('selected'); ?>>N200</option>
												<option value="N80" <?php if (htmlspecialchars(explode(",", $coilInfo['wire_types'])[3]) == 'N80') echo htmlentities('selected'); ?>>N80</option>
												<option value="Stainless Steel" <?php if (htmlspecialchars(explode(",", $coilInfo['wire_types'])[3]) == 'Stainless Steel') echo htmlentities('selected'); ?>>Stainless Steel</option>
												<option value="Titanium" <?php if (htmlspecialchars(explode(",", $coilInfo['wire_types'])[3]) == 'Titanium') echo htmlentities('selected'); ?>>Titanium</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<!-- Wire Five -->
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 multi_selector">
								<div class="form-group">
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 main">
										<div class="form-group">
											<label for="wire_five_gauge">Wire Five</label>
											<input id="wire_five_gauge" class="form-control" type="number" placeholder="AWG" name="edit_wire_five_gauge" value="<?php echo htmlspecialchars(explode(",", $coilInfo['wire_gauges'])[4]); ?>" step="1" min="0" max="99">
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 secondary">
										<div class="form-group">
											<label for="wire_five_type">&nbsp;</label>
											<div class="dropdown_arrow">&#9662;</div>
											<select name="edit_wire_five_type" id="" class="form-control">
												<option value="Kanthal" <?php if (htmlspecialchars(explode(",", $coilInfo['wire_types'])[4]) == 'Kanthal') echo htmlentities('selected'); ?>>Kanthal</option>
												<option value="N200" <?php if (htmlspecialchars(explode(",", $coilInfo['wire_types'])[4]) == 'N200') echo htmlentities('selected'); ?>>N200</option>
												<option value="N80" <?php if (htmlspecialchars(explode(",", $coilInfo['wire_types'])[4]) == 'N80') echo htmlentities('selected'); ?>>N80</option>
												<option value="Stainless Steel" <?php if (htmlspecialchars(explode(",", $coilInfo['wire_types'])[4]) == 'Stainless Steel') echo htmlentities('selected'); ?>>Stainless Steel</option>
												<option value="Titanium" <?php if (htmlspecialchars(explode(",", $coilInfo['wire_types'])[4]) == 'Titanium') echo htmlentities('selected'); ?>>Titanium</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<!-- Wire Six -->
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 multi_selector">
								<div class="form-group">
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 main">
										<div class="form-group">
											<label for="wire_six_gauge">Wire Six</label>
											<input id="wire_six_gauge" class="form-control" type="number" placeholder="AWG" name="edit_wire_six_gauge" value="<?php echo htmlspecialchars(explode(",", $coilInfo['wire_gauges'])[5]); ?>" step="1" min="0" max="99">
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 secondary">
										<div class="form-group">
											<label for="wire_six_type">&nbsp;</label>
											<div class="dropdown_arrow">&#9662;</div>
											<select name="edit_wire_six_type" id="" class="form-control">
												<option value="Kanthal" <?php if (htmlspecialchars(explode(",", $coilInfo['wire_types'])[5]) == 'Kanthal') echo htmlentities('selected'); ?>>Kanthal</option>
												<option value="N200" <?php if (htmlspecialchars(explode(",", $coilInfo['wire_types'])[5]) == 'N200') echo htmlentities('selected'); ?>>N200</option>
												<option value="N80" <?php if (htmlspecialchars(explode(",", $coilInfo['wire_types'])[5]) == 'N80') echo htmlentities('selected'); ?>>N80</option>
												<option value="Stainless Steel" <?php if (htmlspecialchars(explode(",", $coilInfo['wire_types'])[5]) == 'Stainless Steel') echo htmlentities('selected'); ?>>Stainless Steel</option>
												<option value="Titanium" <?php if (htmlspecialchars(explode(",", $coilInfo['wire_types'])[5]) == 'Titanium') echo htmlentities('selected'); ?>>Titanium</option>
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
				<div class="modal-footer" style="padding:10px 50px">	
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 left-no-padding">
						<h4 class="required_note">* Required Field</h4>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 right-no-padding">
						<button type="button" class="btn btn-success btn-block export_edit" name="edit_submit">Edit Coil</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>

	<!-- Flag Modal -->
	<div class="modal fade flag_modal" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<div class="modal-header" style="padding:10px 50px;">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<?php if (isset($_SESSION['coil_flag_error'])) { ?>
					<h4 class="error"><?php echo htmlspecialchars($_SESSION['coil_flag_error']);?></h4>
					<?php } else { ?>
					<h4>Report this coil</h4>
					<?php } ?>
				</div>
				<div class="modal-body" style="padding:15px 50px;">
					<div class="row">
						<form action="" method="post" data-toggle="validator">
							<div class="modal-body">
								<div class="form-group">
									<label for="coil_flag_reason">Reason for Report</label>
									<input type="text" class="form-control" id="coil_flag_reason" placeholder="Reason for flag" name="coil_flag_reason" required value="<?php echo htmlspecialchars($_POST['coil_flag_reason']); ?>">
								</div>	
							</div>
							<div class="modal-footer" style="padding:15px 15px 0 15px;">	
								<button type="submit" class="btn btn-success btn-block" name="coil_flag_submit">Report Coil</button>
							</div>
						</form>
	        		</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Image Modal -->
	<div class="modal fade image_modal" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<div class="modal-header" style="padding:10px 50px;">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4><?php echo htmlentities($coilInfo['coil_name']); ?></h4>
				</div>
				<div class="modal-body" style="padding:15px 50px;">
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12">
							<img src="<?php echo htmlspecialchars($coilInfo['image']); ?>" alt="" class="img-responsive">
						</div>
	        		</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Coil Header -->
	<div id="coil_header" class="text-center">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<h1><?php echo htmlspecialchars($coilInfo['coil_name']); ?></h1>
					<h6>Uploaded By: <a class="user" href="account.php?username=<?php echo htmlspecialchars($coilInfo['username']); ?>&page=1"><?php echo htmlspecialchars($coilInfo['username']); ?></a></h6>
					<?php if (Coil::checkCoilOwner($_SESSION['session_id'], $_GET['coil_id'])) { ?>
					<hr>
					<div id="coil_settings">
						<a data-toggle="modal" data-target=".edit_modal">
							<h4 class="edit"><span class="glyphicon glyphicon-pencil"></span> Edit Coil</h4>
						</a>
						<a data-toggle="modal" data-target=".delete_modal">
							<h4 class="delete"><span class="glyphicon glyphicon-trash"></span> Delete Coil</h4>
						</a>
					</div>
					<?php } ?>
				</div>
			</div>
			<hr>
		</div>
	</div>

	<!-- Coil Image -->
	<div id="coil_image">
		<div class="container">
			<div class="row">
				<div class="col-lg-4"></div>
				<div class="col-lg-4">
					<a data-toggle="modal" data-target=".image_modal">
						<img src="<?php echo htmlspecialchars($coilInfo['image']); ?>" alt="" class="img-responsive img-circle">
					</a>
				</div>
				<div class="col-lg-4"></div>
			</div>
			<hr>
		</div>
	</div>

	<!-- Coil Details -->
	<div id="coil_details">
		<div class="container">
			<div class="row text-center">
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
					<img src="images/wraps.png" alt="" class="img-responsive">
					<h4><?php echo htmlspecialchars($coilInfo['wraps']); ?> Wraps</h4>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
					<img src="images/diameter.png" alt="" class="img-responsive">
					<h4><?php echo htmlspecialchars($coilInfo['inner_diameter']); ?></h4>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
					<img src="images/resistance.png" alt="" class="img-responsive">
					<h4><?php echo htmlspecialchars($coilInfo['resistance']); ?></h4>
				</div>

			</div>
			<hr>
		</div>
	</div>

	<!-- Coil Wire -->
	<div id="coil_wire">
		<div class="container">
			<div class="row text-center">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<h4><?php echo htmlspecialchars($coilInfo['wire']);?></h4>
				</div>
			</div>
			<hr>
		</div>
	</div>

	<!-- Coil More Details -->
	<div id="coil_details">
		<div class="container">
			<div class="row text-center">
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
					<img src="images/atomizer.png" alt="" class="img-responsive">
					<h4><?php echo htmlspecialchars($coilInfo['atomizer']); ?></h4>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
					<img src="images/coil.png" alt="" class="img-responsive">
					<?php if ($coilInfo['coils'] == 0) { ?>
						<h4><?php echo number_format(htmlspecialchars($coilInfo['coils'])); ?> Coils</h4>
					<?php } else if ($coilInfo['coils'] == 1) {?>
						<h4><?php echo number_format(htmlspecialchars($coilInfo['coils'])); ?> Coil</h4>
					<?php } else { ?>
						<h4><?php echo number_format(htmlspecialchars($coilInfo['coils'])); ?> Coils</h4>
					<?php } ?>
					</h4>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
					<img src="images/difficulty.png" alt="" class="img-responsive">
					<h4><?php echo htmlspecialchars($coilInfo['difficulty']); ?></h4>
				</div>
			</div>
			<hr>
		</div>
	</div>

	<!-- Coil Description -->
	<div id="coil_description">
		<div class="container">
			<div class="row text-center">
				<div class="col-lg-12">
					<h1>About this Coil</h1>
				</div>
				<div class="col-lg-1"></div>
				<div class="col-lg-10 pull-center">
					<p><?php echo htmlspecialchars($coilInfo['description']);?></p>
				</div>
				<div class="col-lg-1"></div>
			</div>
			<hr>
		</div>
	</div>

	<!-- Coil Statistics -->
	<div id="coil_statistics">
		<div class="container">
			<div class="row text-center">
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
					<?php if ($coilInfo['views'] == 0) { ?>
						<h4><?php echo number_format(htmlspecialchars($coilInfo['views'])); ?> Views</h4>
					<?php } else if ($coilInfo['views'] == 1) {?>
						<h4><?php echo number_format(htmlspecialchars($coilInfo['views'])); ?> View</h4>
					<?php } else { ?>
						<h4><?php echo number_format(htmlspecialchars($coilInfo['views'])); ?> Views</h4>
					<?php } ?>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 social_interaction">
					<a data-toggle="modal" data-target=".share_modal">
						<h4>Share</h4>
					</a>
					&nbsp;&nbsp;|&nbsp;&nbsp;
					<a data-toggle="modal" data-target=".flag_modal">
						<h4>Report</h4>
					</a>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 ratings_container">
					<div id="ratings">
						<form action="" method="post">
							<div id="likes">
								<button type="submit" name="like">
									<span class="glyphicon glyphicon-arrow-up <?php echo htmlentities(Coil::checkIfLikedOrDisliked($_SESSION['session_id'], $_GET['coil_id'])['like']); ?>"></span>
								</button>
								<h4><?php echo htmlspecialchars(number_format($coilInfo['likes'])); ?></h4>
							</div>
							<div id="dislikes">
								<button type="submit" name="dislike">
									<span class="glyphicon glyphicon-arrow-down <?php echo htmlentities(Coil::checkIfLikedOrDisliked($_SESSION['session_id'], $_GET['coil_id'])['dislike']); ?>"></span>
								</button>
								<h4><?php echo htmlspecialchars(number_format($coilInfo['dislikes'])); ?></h4>
							</div>
						</form>
					</div>
				</div>
			</div>
			<hr>
		</div>
	</div>

	<!-- Coil Comment Form -->
	<div id="coil_comment_form">
		<div class="container">
			<div class="row text-center">
				<h1><?php echo htmlspecialchars($coil->getNumberOfComments($_GET['coil_id'])); ?></h1>
				<form method="post" action="">
					<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"></div>
					<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
						<div class="input-group">
							<textarea name="coil_comment_comment" maxlength="512" class="form-control custom-control comment_textarea"></textarea>
							<span class="input-group-addon btn btn-primary">
								<input type="submit" name="coil_comment_submit" value="Post">
							</span>
						</div>
					</div>
					<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"></div>
				</form>
			</div>
			<hr>
		</div>
	</div>

	<!-- Coil Comments List -->
	<div id="coil_comment_list">
		<div class="container">
			<div class="row">
				<?php foreach (Coil::listComments($_GET['coil_id'], $_GET['page']) as $data) : ?>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 comment_container">
						<div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
							<a href="account.php?username=<?php echo htmlspecialchars($data['username']); ?>&page=1">
								<img src="users/<?php echo htmlspecialchars($data['username']); ?>/profile_picture.jpg" class="img-responsive img-circle">
							</a>
						</div>
						<div class="col-lg-11 col-md-11 col-sm-10 col-xs-10 jumbotron">
								<h6><a href="account.php?username=<?php echo htmlspecialchars($data['username']); ?>&page=1"><?php echo htmlspecialchars($data['username']) . "</a> • " . $data['date_added']; ?></h6>
							<p><?php echo htmlspecialchars($data['comment']); ?></p>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>

	<!-- Coil Pagination -->
	<div id="coil_comment_pagination" class="text-center">
		<div class="container">
			<!-- Verify there are comments -->
			<?php if ($coil->getNumberOfComments($_GET['coil_id']) >= 1) { ?>
				<div class="pagination">
					<!-- If page is eqaul to first don't display last button -->
					<?php if ($_GET['page'] > 1) { ?>
					<li><a href="coil.php?coil_id=<?php echo htmlspecialchars($_GET['coil_id']); ?>&page=<?php echo htmlspecialchars($_GET['page']) - 1; ?>">Last</a></li>
					<?php } ?>
					<!-- List numerical pages -->
					<?php for($x = 1; $x <= Coil::commentPagination($_GET['coil_id'], $_GET['page']); $x++) : ?>
						<li><a href="coil.php?coil_id=<?php echo htmlspecialchars($_GET['coil_id']); ?>&page=<?php echo htmlspecialchars($x); ?>"<?php if($_GET['page'] == $x) {echo htmlspecialchars("class=selected");}?>><?php echo htmlspecialchars($x); ?></a></li>
					<?php endfor; ?>
					<!-- If page is eqaul to last don't display next button -->
					<?php if ($_GET['page'] < Coil::commentPagination($_GET['coil_id'], $_GET['page'])) { ?>
					<li><a href="coil.php?coil_id=<?php echo htmlspecialchars($_GET['coil_id']); ?>&page=<?php echo htmlspecialchars($_GET['page']) + 1; ?>">Next</a></li>
					<?php } ?>
				</div>
				<?php } ?>
		</div>
	</div>

	<!-- Footer -->
	<?php require 'templates/footer.php' ?>
</body>
</html>