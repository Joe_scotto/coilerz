<?php

class Account {
	/**
	 * Gives user non critical user information based on $_GET username
	 * @param  string $username Username $_GET variable
	 * @return array Containing username, date_joined, coils from the users table
	 */
	public function getUserPublicInfo ($username) {
		//Database Connection
		$db = Database::getInstance();

		//Grab everything from database where session_id matches
		$query = $db->getConnection()->prepare("SELECT username, date_joined, coils FROM `users` WHERE username = :username");
		$query->execute(array(
			':username' => $username
		));

		//Return results in associative array
		return $query->fetch(PDO::FETCH_ASSOC);
	}

	/**
	 * 
	 * Gives all user information based on the session_id
	 * @param  string $session_id Session ID of the logged in user
	 * @return array Containing All information where the session_id is found
	 */
	public static function getUserPersonalInfo ($session_id) {
		//Database Connection
		$db = Database::getInstance();

		//Grab everything from database where session_id matches
		$query = $db->getConnection()->prepare("SELECT * FROM `users` WHERE session_id = :session_id");
		$query->execute(array(
			':session_id' => $session_id
		));

		//Return results in associative array
		return $query->fetch(PDO::FETCH_ASSOC);
	}

	/**
	 * Updates the users profile picture
	 * @param  string $session_id ID of the logged in user
	 * @param  mixed $data $_POST data
	 * @return string Error if failed, refresh if updated correctly
	 */
	public function accountSettingsUpdateProfilePicture ($session_id, $data) {
		//Database Connection 
		$db = Database::getInstance();

		$base64 = $data['account_settings_profile_picture'];
		$directory = 'users/' . $this->getUserPersonalInfo($session_id)['username'] . '/profile_picture.jpg';

		$db->decodeBase64($base64, $directory);
	}

	/**
	 * Updates the users email
	 * @param  string $session_id ID of the logged in user
	 * @param  mixed $data $_POST data
	 * @return string Error if failed, refresh if updated correctly
	 */
	public function accountSettingsUpdateEmail ($session_id, $data) {
		//Database Connection 
		$db = Database::getInstance();

		//Select password from database where session_id matches
		$query = $db->getConnection()->prepare("SELECT password FROM `users` WHERE session_id = :session_id");
		$query->execute(array(
			':session_id' => $session_id
		));

		//Return Results in Array
		$results = $query->fetch(PDO::FETCH_ASSOC);

		//Validate Password
		if (password_verify($data['account_settings_current_password'], $results['password'])) {
			//Prepare Query
				$query = $db->getConnection()->prepare("UPDATE `users` SET email = :email WHERE session_id = :session_id");

				//Run Query
				$query->execute(array(
					':session_id' => $session_id,
					':email' => $data['account_settings_email']
				));
		} else {
			$_SESSION['account_settings_error'] = "Incorrect Current Password";
		}
	}

	/**
	 * Updates the users password
	 * @param  string $session_id ID of the logged in user
	 * @param  mixed $data $_POST data
	 * @return string Error if failed, refresh if updated correctly
	 */
	public function accountSettingsUpdatePassword ($session_id, $data, $url) {
		//Database Connection 
		$db = Database::getInstance();

		//Select password from database where session_id matches
		$query = $db->getConnection()->prepare("SELECT password FROM `users` WHERE session_id = :session_id");
		$query->execute(array(
			':session_id' => $session_id
		));

		//Return Results in Array
		$results = $query->fetch(PDO::FETCH_ASSOC);

		//Validate Password
		if (password_verify($data['account_settings_current_password'], $results['password'])) {
				//Define Query Values
				$password = $db->hashPassword($data['account_settings_password']);

				//Prepare Query
				$query = $db->getConnection()->prepare("UPDATE `users` SET password = :password WHERE session_id = :session_id");

				//Run Query
				$query->execute(array(
					':session_id' => $session_id,
					':password' => $password
				));

				//Set Cookie
				setcookie('sessionpersist', 1);

				//Logout user
				$registration = new Registration();
				$registration->logout($url, "Password was updated successfully, please login.");
		} else {
			$_SESSION['account_settings_error'] = "Incorrect Current Password";
		}
	}

	/**
	 * Updates the users email preferences
	 * @param  string $session_id ID of the logged in user
	 * @param  mixed $data $_POST data
	 * @return string Error if failed, refresh if updated correctly
	 */
	public function accountSettingsUpdateNewsletter ($session_id, $data, $url) {
		//Database Connection
		$db = Database::getInstance();

		//Query
		$query = $db->getConnection()->prepare("UPDATE `users` SET newsletter = :newsletter WHERE session_id = :session_id");
		$query->execute(array(
			':newsletter' => $data,
			':session_id' => $session_id
		));
	}

	/**
	 * Returns the coils for the specified users
	 * @param  string $username Username of the user
	 * @return array Coils found for that coil
	 */
	public static function listUsersCoils ($username, $page) {
		//Database Connection
		$db = Database::getInstance();

		//User Input
		$page = isset($page) ? (int)$page : 1;
		$perPage = 12;

		//Positioning
		$start = ($page > 1) ? ($page * $perPage) - $perPage : 0;

		//Select comments from database based on $coil_id
		$query = $db->getConnection()->prepare("SELECT * FROM `coils` WHERE username = :username ORDER BY id DESC LIMIT {$start}, {$perPage}");
		$query->execute(array(
			':username' => $username
		));

		//Return results
		return $query;
	}

	/**
	 * Pagination for users coils
	 * @param  string $username Username of the user
	 * @param  int $page Current page
	 * @return int Number of pages
	 */
	public static function listUserPagination ($username, $page) {
		//Database Connection
		$db = Database::getInstance();

		//User Input
		$page = isset($page) ? (int)$page : 1;
		$perPage = 12;

		//Set page to one if lower
		$start = ($page > 1) ? ($page * $perPage) - $perPage : 0;

		//Query to get total
		$totalQuery = $db->getConnection()->prepare("SELECT id FROM `coils` WHERE username = :username");
		$totalQuery->execute(array(
			':username' => $username
		));

		//Total number of pages
		$total = $totalQuery->rowCount();
		$pages = ceil($total / $perPage);

		return $pages;
	}

	/**
	 * Returns the number of coils for the specified users
	 * @param  string $username Username of the user
	 * @return array Coils found for that coil
	 */
	public static function listNumberOfCoils ($username) {
		//Database Connection
		$db = Database::getInstance();

		//Select coils from database based on $username
		$query = $db->getConnection()->prepare("SELECT id FROM `coils` WHERE username = :username");
		$query->execute(array(
			':username' => $username
		));

		//Return results
		return $query->rowCount();
	}

	/**
	 * Checks if the user exists
	 * @param  string $username Name of the user
	 * @return bool 
	 */
	public static function checkIfUserExists ($username) {
		//Database Connection
		$db = Database::getInstance();

		//Select coils from database based on $username
		$query = $db->getConnection()->prepare("SELECT id FROM `users` WHERE username = :username");
		$query->execute(array(
			':username' => $username
		));

		//Return results
		if ($query->rowCount() <= 0) {
			header("Location: 404.php");
			die();
		}
	}
}