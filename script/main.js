//Get Variables Function
function getQueryVariable (variable) {
       var query = window.location.search.substring(1);
       var vars = query.split("&");

       for (var i=0;i<vars.length;i++) {
           var pair = vars[i].split("=");
           if(pair[0] == variable){return pair[1];}
       }

       return(false);
}

//Enable Bootstrap Tooltips
$("[data-toggle='tooltip']").tooltip();

//Adds classes based on screen width
function verifyWidth () {
	if ($(window).width() < 768) {
        $("#hide").addClass("hide");
        $(".navlapse").removeClass("pull-right");
        $(".navlapse").addClass("text-center");

        $("#last_header").removeClass('last_header');

        $("#featured_section .coil").addClass("pull-center");
		$("#most_viewed_section .coil").addClass("pull-center");

		$(".top").addClass("text-center");
		$(".top ul").addClass("small_view");
		$(".top h1").addClass("small_h1");
		$(".top").addClass("margin_top");

		$(".logout_button").addClass("logout_button_small");
    } else {
		$("#hide").removeClass("hide");
		$(".navlapse").removeClass("text-center");
		$(".navlapse").addClass("pull-right");

		$("#last_header").addClass('last_header');

		$("#featured_section .coil").removeClass("pull-center");
		$("#most_viewed_section .coil").removeClass("pull-center");

		$(".top").removeClass("text-center");
		$(".top ul").removeClass("small_view");
		$(".top h1").removeClass("small_h1");
		$(".top").removeClass("margin_top");

		$(".logout_button").removeClass("logout_button_small");
    }
}

window.onresize = verifyWidth;
window.onload = verifyWidth;

//Change Most Viewed Builds
	//Today
	$("#most_viewed_section #today").click(function (e) {
		//Add Underline
		$(this).css("text-decoration", "underline");
		$("#most_viewed_section #week, #month, #time").css("text-decoration", "none");

		$("#most_day").css("display", "block");
		$("#most_week, #most_month, #most_time").css("display", "none");
	});

	//This Week
	$("#most_viewed_section #week").click(function (e) {
		//Add Underline
		$(this).css("text-decoration", "underline");
		$("#most_viewed_section #today, #month, #time").css("text-decoration", "none");

		$("#most_week").css("display", "block");
		$("#most_day, #most_month, #most_time").css("display", "none");
	});

	//This Month
	$("#most_viewed_section #month").click(function (e) {
		//Add Underline
		$(this).css("text-decoration", "underline");
		$("#most_viewed_section #today, #week, #time").css("text-decoration", "none");

		$("#most_month").css("display", "block");
		$("#most_day, #most_week, #most_time").css("display", "none");
	});

	//All Time
	$("#most_viewed_section #time").click(function (e) {
		//Add Underline
		$(this).css("text-decoration", "underline");
		$("#most_viewed_section #today, #month, #week").css("text-decoration", "none");

		$("#most_time").css("display", "block");
		$("#most_day, #most_month, #most_week").css("display", "none");
	});	

//Change Highest Rated Builds 
	//Today
	$("#highest_rated_section #today").click(function (e) {
		//Add Underline
		$(this).css("text-decoration", "underline");
		$("#highest_rated_section #week, #month, #time").css("text-decoration", "none");

		$("#highest_rated_section #this_day").css("display", "block");
		$("#highest_rated_section #this_week, #this_month, #all_time").css("display", "none");
	});

	//This Week
	$("#highest_rated_section #week").click(function (e) {
		//Add Underline
		$(this).css("text-decoration", "underline");
		$("#highest_rated_section #today, #month, #time").css("text-decoration", "none");

		$("#highest_rated_section #this_week").css("display", "block");
		$("#highest_rated_section #this_day, #this_month, #all_time").css("display", "none");
	});

	//This Month
	$("#highest_rated_section #month").click(function (e) {
		//Add Underline
		$(this).css("text-decoration", "underline");
		$("#highest_rated_section #today, #week, #time").css("text-decoration", "none");

		$("#highest_rated_section #this_month").css("display", "block");
		$("#highest_rated_section #this_day, #this_week, #all_time").css("display", "none");
	});

	//All Time
	$("#highest_rated_section #time").click(function (e) {
		//Add Underline
		$(this).css("text-decoration", "underline");
		$("#highest_rated_section #today, #month, #week").css("text-decoration", "none");

		$("#highest_rated_section #all_time").css("display", "block");
		$("#highest_rated_section #this_day, #this_month, #this_week").css("display", "none");
	});

//Hide menu items
	$(".login_button, .sign_up_button, .contact_button, .upload_button").click(function (e) {
		$(".navbar-collapse").removeClass("in");
	});

//CropIt
	//Upload Form
		//Initiate Cropper
		$('.image-editor-upload').cropit({
			exportZoom: 3
		});

		//Assign Export Function to Upload Form
		$('.export_upload').click(function() {
			$("#upload_form_identifier").val("upload_form");

			var imageData = $('.image-editor-upload').cropit('export', {
				type: 'image/jpeg',
				quality: .75,
				originalSize: false
			});

			//Set value of hidden input to base64
			$("#hidden_base64_upload").val(imageData);

			//Pause form submission until input is populated
			window.setTimeout(function() {
				document.upload_form.submit();
			}, 1000);
		});

	//Account Settings Form 
		//Initiate Cropper
		$('.image-editor-account-settings').cropit({
			exportZoom: 3,
			imageState: {
				src:'users/' + getQueryVariable("username") + '/profile_picture.jpg'
			}}
		);

		//Assign Export Function to Account Settings Form
		$('.export_account_settings').click(function() {
			$("#account_settings_form_identifier").val("account_settings");

			var imageData = $('.image-editor-account-settings').cropit('export', {
		        type: 'image/jpeg',
		        quality: 0.75,
		        originalSize: false,
			});

			//Set value of hidden input to base64
			$("#hidden_base64_account_settings").val(imageData);

			//Pause form submission until input is populated
			window.setTimeout(function() {
				document.account_settings_form.submit();
			}, 1000);
		});
	//Edit Form
		//Initiate Cropper
		$('.image-editor-edit').cropit({
			exportZoom: 3,
			imageState: {
				src:'coils/' + getQueryVariable("coil_id") + '/coil_image.jpeg'
			}
		});

		//Assign Export Function to Upload Form
		$('.export_edit').click(function() {
			$("#edit_form_identifier").val("edit_form");

			var imageData = $('.image-editor-edit').cropit('export', {
				type: 'image/jpeg',
				quality: .75,
				originalSize: false
			});

			//Set value of hidden input to base64
			$("#hidden_base64_edit").val(imageData);

			//Pause form submission until input is populated
			window.setTimeout(function() {
				document.edit_form.submit();
			}, 1000);
		});

//Social Popup Function
function popup(url,windowName) {
	newWindow=window.open(url,windowName,'height=300,width=700');

	if (window.focus) {
		newWindow.focus();
	}

	return false;
}