<?php require 'core/init.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<!-- Bootstrap Mobile Optimization -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<!-- Meta Tags -->
	<meta name="description" content="Coilerz is a website designed for vapers who want to share their build or find other builds." />
	<title>Coilerz - Find and share vape coil builds</title>
	<!-- Favicon -->
	<link rel="icon" href="images/logo.png">
	<!-- Main Stylesheet -->
	<link rel="stylesheet" href="css/style.css">
	<!-- Bootstrap CDN CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
</head>

<body>
	<!-- Header and Search -->
	<div id="main_section">
		<div class="container">
			<!-- Navigation and Logo -->
			<nav class="navbar navbar-default">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="index.php">COILERZ</a>
				</div>
				<div class="collapse navbar-collapse pull-right navlapse" id="collapse">
					<ul class="nav navbar-nav" id="navigation">
						<li><a href="about.php">About</a></li>
						<li><a class="contact_button" data-toggle="modal" data-target=".contact_modal">Contact</a></li>

						<!-- Displays only if user is logged in -->
						<?php if($registration->checkLogin($_SESSION['session_id'])) { ?>
						<li><a class="upload_button" data-toggle="modal" data-target=".upload_modal">Upload</a></li>
						<?php }?>

						<li id="hide"><a>|</a></li>

						<!-- Displays only if user is logged in -->
						<?php if($registration->checkLogin($_SESSION['session_id'])) { ?>
						<li><a href="account.php?username=<?php echo htmlspecialchars(Account::getUserPersonalInfo($_SESSION['session_id'])['username']); ?>&page=1">Account</a></li>
						<li>
							<form method="post" action="">
								<a><button type="submit" name="logout" class="logout_button">&nbsp;Logout</button></a>
							</form>
						</li>

						<!-- Displays if user is not logged in -->
						<?php } else { ?>
						<li><a class="login_button" data-toggle="modal" data-target=".login_modal">Login</a></li>
						<li><a class="last_header sign_up_button" id="last_header" data-toggle="modal" data-target=".sign_up_modal">Sign Up</a></li>
						<?php }?>
					</ul>
				</div>
			</nav>
			
			<!-- Search Form -->
			<div id="search_container">
				<img src="images/logo.png">
				<h3 class="text-center">Find and share vape coil builds.</h3>
				<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 pull-center">
					<form action="" method="post">
						<div class="input-group">
							<input type="text" class="form-control" placeholder="Search for a coil, wire, or atomizer" name="search_term">
							<span class="input-group-btn">
								<button class="btn btn-default" type="submit" name="search_submit">
									<span class="glyphicon glyphicon-search"></span>
								</button>
							</span>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<!-- Login Modal -->
	<div class="modal fade login_modal" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<div class="modal-header" style="padding:10px 50px;">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<?php if (isset($_SESSION['login_error'])) { ?>
					<h4 class="error"><?php echo htmlspecialchars($_SESSION['login_error']);?></h4>
					<?php } else { ?>
					<h4>Login</h4>
					<?php }?>
				</div>
				<form action="" method="post" data-toggle="validator">
					<div class="modal-body" style="padding:15px 50px;">
						<div class="form-group">
							<label for="login_username">Username</label>
							<input type="text" class="form-control" id="login_username" placeholder="Username" name="login_username" maxlength="18" required value="<?php echo htmlspecialchars($_POST['login_username']); ?>">
						</div>
						<div class="form-group">
							<label for="login_password">Password</label>
							<input type="password" class="form-control" id="login_password" placeholder="Password" name="login_password" required>
						</div>			
					</div>
					<div class="modal-footer" style="padding:10px 50px">
						<div class="checkbox pull-left">
							<label><input type="checkbox" name="login_remember_me">Remember me</label>
						</div>	
						<button type="submit" class="btn btn-success" name="login_submit">Login</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<!-- Sign Up Modal -->
	<div class="modal fade sign_up_modal" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<div class="modal-header" style="padding:10px 50px;">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<?php if (isset($_SESSION['sign_up_error'])) { ?>
					<h4 class="error"><?php echo htmlspecialchars($_SESSION['sign_up_error']);?></h4>
					<?php } else { ?>
					<h4>Sign Up</h4>
					<?php } ?>
				</div>
				<form action="" method="post" data-toggle="validator">
					<div class="modal-body" style="padding:15px 50px;">
						<div class="form-group">
							<label for="sign_up_email">Email</label>
							<input type="email" class="form-control" id="sign_up_email" placeholder="Email" name="sign_up_email" maxlength="512" required data-error="Please enter a valid email" value="<?php echo htmlspecialchars($_POST['sign_up_email']); ?>">
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group">
							<label for="sign_up_username">Username (Letters, Numbers, Dashes, and/or underscores)</label>
							<input type="text" class="form-control" id="sign_up_username" placeholder="Username" name="sign_up_username" maxlength="18" required value="<?php echo htmlspecialchars($_POST['sign_up_username']); ?>">
						</div>
						<div class="form-group">
							<label for="sign_up_password">Password</label>
							<input type="password" class="form-control" id="sign_up_password" placeholder="Password" name="sign_up_password" required>
						</div>	
						<div class="form-group">
							<label for="sign_up_confirm_password">Confirm Password</label>
							<input type="password" class="form-control" id="sign_up_confirm_password" placeholder="Confirm Password" name="sign_up_confirm_password" required data-error="Whoa, these don't match" data-match="#sign_up_password">
							<div class="help-block with-errors"></div>
						</div>		
					</div>
					<div class="modal-footer" style="padding:10px 50px">
						<div class="checkbox pull-left">
							<label>
								<input type="checkbox" name="sign_up_terms" required>
								I agree to the <a href="terms.php">Terms</a>
							</label>
						</div>
						<div class="checkbox pull-left" id="sign_up_newletter">
							<label>
								<input type="checkbox" name="sign_up_newsletter">
								Sign me up for the newsletter
							</label>
						</div>
						<button type="submit" class="btn btn-success sign_up_form_button" name="sign_up_submit">Sign Up</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<!-- Contact Modal -->
	<div class="modal fade contact_modal" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<div class="modal-header" style="padding:10px 50px;">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<?php if (isset($_SESSION['contact_error'])) { ?>
					<h4 class="error"><?php echo htmlspecialchars($_SESSION['contact_error']);?></h4>
					<?php } else { ?>
					<h4>Contact</h4>
					<?php }?>
				</div>
				<form action="" method="post" data-toggle="validator">
					<div class="modal-body" style="padding:15px 50px;">
						<div class="form-group">
							<label for="contact_name">Your Name</label>
							<input type="text" class="form-control" id="contact_name" placeholder="Your Name" name="contact_name" required value="<?php echo htmlspecialchars($_POST['contact_name']); ?>">
						</div>
						<div class="form-group">
							<label for="contact_email">Your Email</label>
							<input type="email" class="form-control" id="contact_email" placeholder="Your Email" name="contact_email" data-error="Please enter a valid email" required value="<?php echo htmlspecialchars($_POST['contact_email']); ?>">
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group">
							<label for="contact_subject">Subject</label>
							<input type="text" class="form-control" id="contact_subject" placeholder="Subject" name="contact_subject" required value="<?php echo htmlspecialchars($_POST['contact_subject']); ?>">
						</div>	
						<div class="form-group">
							<label for="contact_message">Message</label>
							<textarea name="contact_message" id="contact_message" class="form-control" rows="5" required><?php echo htmlspecialchars($_POST['contact_message']); ?></textarea>
						</div>			
					</div>
					<div class="modal-footer" style="padding:10px 50px">	
						<button type="submit" class="btn btn-success btn-block" name="contact_submit">Send Message</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<!-- Upload Modal -->
	<div class="modal fade upload_modal" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<div class="modal-header" style="padding:10px 50px;">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<?php if (isset($_SESSION['upload_error'])) { ?>
					<h4 class="error"><?php echo htmlspecialchars($_SESSION['upload_error']);?></h4>
					<?php } else { ?>
					<h4>Upload Coil</h4>
					<?php }?>
				</div>
				<form action="" method="post" data-toggle="validator" name="upload_form" id="upload_form" enctype="multipart/form-data">
					<div class="modal-body" style="padding:15px 50px;">
						<div class="row">
							<!-- Hidden Identifier -->
							<input type="text" name="upload_form_identifier" id="upload_form_identifier" value="<?php echo htmlspecialchars($_POST['upload_form_identifier']); ?>">
							<!-- Coil Name -->
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<div class="form-group">
									<label for="coil_name">Coil Name*</label>
									<input type="text" class="form-control" id="coil_name" placeholder="Coil Name" name="upload_coil_name" value="<?php echo htmlspecialchars($_POST['upload_coil_name']); ?>" maxlength="32" required>
								</div>
							</div>
							<!-- Resistance -->
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<div class="form-group">
									<label for="resistance">Resistance*</label>
									<div class="input-group">
										<input type="number" class="form-control" id="resistance" placeholder="Resistance" name="upload_resistance" step="0.001" min="0" value="<?php echo htmlspecialchars($_POST['upload_resistance']); ?>"  required>
										<div class="input-group-addon">Ω</div>
									</div>
								</div>
							</div>
							<!-- Inner Diameter -->
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 multi_selector">
								<div class="col-lg-8 col-md-8 col-sm-10 col-xs-10 main">
									<div class="form-group">
										<label for="inner_diameter">Inner Diameter*</label>
										<input id="inner_diameter" class="form-control" type="number" placeholder="Inner Diameter" name="upload_inner_diameter" step="0.00001" min="0" value="<?php echo htmlspecialchars($_POST['upload_inner_diameter']); ?>" required>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-2 col-xs-2 secondary">
									<div class="form-group">
										<label for="upload_inner_diameter_unit">&nbsp;</label>
										<div class="dropdown_arrow">&#9662;</div>
										<select name="upload_inner_diameter_unit" id="" class="form-control" id="upload_inner_diameter_unit" required>
											<option value="mm" <?php if ($_POST['upload_inner_diameter_unit'] == 'mm') echo htmlentities('selected'); ?>>mm</option>
											<option value="in" <?php if ($_POST['upload_inner_diameter_unit'] == 'in') echo htmlentities('selected'); ?>>in</option>
										</select>
									</div>
								</div>
							</div>
							<!-- Wraps -->
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<div class="form-group">
									<label for="wraps">Wraps*</label>
									<input type="number" class="form-control" id="wraps" placeholder="Wraps" name="upload_wraps"  step="1" min="0" value="<?php echo htmlspecialchars($_POST['upload_wraps']); ?>" required>
								</div>
							</div>
							<!-- Number of Coils -->
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<div class="form-group">
									<label for="number_of_coils">Number of Coils*</label>
									<input type="number" class="form-control" id="number_of_coils" placeholder="Number of Coils" name="upload_number_of_coils"  step="1" min="1" max="50" value="<?php echo htmlspecialchars($_POST['upload_number_of_coils']); ?>" required>
								</div>
							</div>
							<!-- Atomizer -->
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<div class="form-group">
									<label for="atomizer">Atomizer</label>
									<input type="text" class="form-control" id="atomizer" placeholder="Atomizer" name="upload_atomizer" value="<?php echo htmlspecialchars($_POST['upload_atomizer']); ?>" maxlength="32">
								</div>
							</div>
							<!-- Difficulty -->
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<label for="">Difficulty*</label>
								<div class="btn-group btn-group-justified" data-toggle="buttons">
									<label class="btn btn-primary <?php if ($_POST['upload_difficulty'] == 'easy') echo htmlentities('active'); ?>">
										<input type="radio" name="upload_difficulty" id="easy" value="easy" <?php if ($_POST['upload_difficulty'] == 'easy') echo htmlentities('checked'); ?>>Easy
									</label>
									<label class="btn btn-primary <?php if ($_POST['upload_difficulty'] == 'medium') echo htmlentities('active'); ?>">
										<input type="radio" name="upload_difficulty" id="medium" value="medium" <?php if ($_POST['upload_difficulty'] == 'medium') echo htmlentities('checked'); ?>>Medium
									</label>
									<label class="btn btn-primary <?php if ($_POST['upload_difficulty'] == 'hard') echo htmlentities('active'); ?>">
										<input type="radio" name="upload_difficulty" id="hard" value="hard" <?php if ($_POST['upload_difficulty'] == 'hard') echo htmlentities('checked'); ?>>Hard
									</label>
									<label class="btn btn-primary <?php if ($_POST['upload_difficulty'] == 'experimental') echo htmlentities('active'); ?>">
										<input type="radio" name="upload_difficulty" id="experimental" value="experimental" <?php if ($_POST['upload_difficulty'] == 'experimental') echo htmlentities('checked'); ?>>Experimental
									</label>
								</div>
							</div>
							<!-- Coil Image -->
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="form-group">
									<label for="wraps">Coil Image*</label>
									<label for="cropit-image-input-upload" id="select_image"  data-toggle="tooltip" data-placement="bottom" title="Must be at least 768x768 and 2mb or less">Select Image</label>
									<hr id="upload_divider">
									<div class="image-editor-upload">
										<input type="file" class="cropit-image-input" id="cropit-image-input-upload" accept="jpg, jpeg, png" name="upload_file">
										<div class="cropit-preview"></div>
										<input type="text" id="hidden_base64_upload" name="upload_image">
										<input type="range" class="cropit-image-zoom-input range">
									</div>
									<hr>
								</div>
							</div>
							<!-- Description -->
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="form-group">
									<label for="upload_description">Description*</label>
									<textarea name="upload_description" class="form-control" id="upload_description" maxlength="512" required><?php echo htmlspecialchars($_POST['upload_description']); ?></textarea>														
									<hr>
								</div>
							</div>
							<!-- Wire One -->
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 multi_selector">
								<div class="form-group">
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 main">
										<div class="form-group">
											<label for="wire_one_gauge">Wire One*</label>
											<input id="wire_one_gauge" class="form-control" type="number" placeholder="AWG" name="upload_wire_one_gauge" value="<?php echo htmlspecialchars($_POST['upload_wire_one_gauge']); ?>" step="1" min="0" max="99" required>
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 secondary">
										<div class="form-group">
											<label for="wire_one_type">&nbsp;</label>
											<div class="dropdown_arrow">&#9662;</div>
											<select name="upload_wire_one_type" id="" class="form-control" required>
												<option value="Kanthal" <?php if ($_POST['upload_wire_one_type'] == 'Kanthal') echo htmlentities('selected'); ?>>Kanthal</option>
												<option value="N200" <?php if ($_POST['upload_wire_one_type'] == 'N200') echo htmlentities('selected'); ?>>N200</option>
												<option value="N80" <?php if ($_POST['upload_wire_one_type'] == 'N80') echo htmlentities('selected'); ?>>N80</option>
												<option value="Stainless Steel" <?php if ($_POST['upload_wire_one_type'] == 'Stainless Steel') echo htmlentities('selected'); ?>>Stainless Steel</option>
												<option value="Titanium" <?php if ($_POST['upload_wire_one_type'] == 'Titanium') echo htmlentities('selected'); ?>>Titanium</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<!-- Wire Two -->
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 multi_selector">
								<div class="form-group">
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 main">
										<div class="form-group">
											<label for="wire_two_gauge">Wire Two</label>
											<input id="wire_two_gauge" class="form-control" type="number" placeholder="AWG" name="upload_wire_two_gauge" value="<?php echo htmlspecialchars($_POST['upload_wire_two_gauge']); ?>" step="1" min="0" max="99">
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 secondary">
										<div class="form-group">
											<label for="wire_two_type">&nbsp;</label>
											<div class="dropdown_arrow">&#9662;</div>
											<select name="upload_wire_two_type" id="" class="form-control">
												<option value="Kanthal" <?php if ($_POST['upload_wire_two_type'] == 'Kanthal') echo htmlentities('selected'); ?>>Kanthal</option>
												<option value="N200" <?php if ($_POST['upload_wire_two_type'] == 'N200') echo htmlentities('selected'); ?>>N200</option>
												<option value="N80" <?php if ($_POST['upload_wire_two_type'] == 'N80') echo htmlentities('selected'); ?>>N80</option>
												<option value="Stainless Steel" <?php if ($_POST['upload_wire_two_type'] == 'Stainless Steel') echo htmlentities('selected'); ?>>Stainless Steel</option>
												<option value="Titanium" <?php if ($_POST['upload_wire_two_type'] == 'Titanium') echo htmlentities('selected'); ?>>Titanium</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<!-- Wire Three -->
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 multi_selector">
								<div class="form-group">
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 main">
										<div class="form-group">
											<label for="wire_three_gauge">Wire Three</label>
											<input id="wire_three_gauge" class="form-control" type="number" placeholder="AWG" name="upload_wire_three_gauge" value="<?php echo htmlspecialchars($_POST['upload_wire_three_gauge']); ?>" step="1" min="0" max="99">
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 secondary">
										<div class="form-group">
											<label for="wire_three_type">&nbsp;</label>
											<div class="dropdown_arrow">&#9662;</div>
											<select name="upload_wire_three_type" id="" class="form-control">
												<option value="Kanthal" <?php if ($_POST['upload_wire_three_type'] == 'Kanthal') echo htmlentities('selected'); ?>>Kanthal</option>
												<option value="N200" <?php if ($_POST['upload_wire_three_type'] == 'N200') echo htmlentities('selected'); ?>>N200</option>
												<option value="N80" <?php if ($_POST['upload_wire_three_type'] == 'N80') echo htmlentities('selected'); ?>>N80</option>
												<option value="Stainless Steel" <?php if ($_POST['upload_wire_three_type'] == 'Stainless Steel') echo htmlentities('selected'); ?>>Stainless Steel</option>
												<option value="Titanium" <?php if ($_POST['upload_wire_three_type'] == 'Titanium') echo htmlentities('selected'); ?>>Titanium</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<!-- Wire Four -->
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 multi_selector">
								<div class="form-group">
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 main">
										<div class="form-group">
											<label for="wire_four_gauge">Wire Four</label>
											<input id="wire_four_gauge" class="form-control" type="number" placeholder="AWG" name="upload_wire_four_gauge" value="<?php echo htmlspecialchars($_POST['upload_wire_four_gauge']); ?>" step="1" min="0" max="99">
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 secondary">
										<div class="form-group">
											<label for="wire_four_type">&nbsp;</label>
											<div class="dropdown_arrow">&#9662;</div>
											<select name="upload_wire_four_type" id="" class="form-control">
												<option value="Kanthal" <?php if ($_POST['upload_wire_four_type'] == 'Kanthal') echo htmlentities('selected'); ?>>Kanthal</option>
												<option value="N200" <?php if ($_POST['upload_wire_four_type'] == 'N200') echo htmlentities('selected'); ?>>N200</option>
												<option value="N80" <?php if ($_POST['upload_wire_four_type'] == 'N80') echo htmlentities('selected'); ?>>N80</option>
												<option value="Stainless Steel" <?php if ($_POST['upload_wire_four_type'] == 'Stainless Steel') echo htmlentities('selected'); ?>>Stainless Steel</option>
												<option value="Titanium" <?php if ($_POST['upload_wire_four_type'] == 'Titanium') echo htmlentities('selected'); ?>>Titanium</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<!-- Wire Five -->
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 multi_selector">
								<div class="form-group">
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 main">
										<div class="form-group">
											<label for="wire_five_gauge">Wire Five</label>
											<input id="wire_five_gauge" class="form-control" type="number" placeholder="AWG" name="upload_wire_five_gauge" value="<?php echo htmlspecialchars($_POST['upload_wire_five_gauge']); ?>" step="1" min="0" max="99">
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 secondary">
										<div class="form-group">
											<label for="wire_five_type">&nbsp;</label>
											<div class="dropdown_arrow">&#9662;</div>
											<select name="upload_wire_five_type" id="" class="form-control">
												<option value="Kanthal" <?php if ($_POST['upload_wire_five_type'] == 'Kanthal') echo htmlentities('selected'); ?>>Kanthal</option>
												<option value="N200" <?php if ($_POST['upload_wire_five_type'] == 'N200') echo htmlentities('selected'); ?>>N200</option>
												<option value="N80" <?php if ($_POST['upload_wire_five_type'] == 'N80') echo htmlentities('selected'); ?>>N80</option>
												<option value="Stainless Steel" <?php if ($_POST['upload_wire_five_type'] == 'Stainless Steel') echo htmlentities('selected'); ?>>Stainless Steel</option>
												<option value="Titanium" <?php if ($_POST['upload_wire_five_type'] == 'Titanium') echo htmlentities('selected'); ?>>Titanium</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<!-- Wire Six -->
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 multi_selector">
								<div class="form-group">
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 main">
										<div class="form-group">
											<label for="wire_six_gauge">Wire Six</label>
											<input id="wire_six_gauge" class="form-control" type="number" placeholder="AWG" name="upload_wire_six_gauge" value="<?php echo htmlspecialchars($_POST['upload_wire_six_gauge']); ?>" step="1" min="0" max="99">
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 secondary">
										<div class="form-group">
											<label for="wire_six_type">&nbsp;</label>
											<div class="dropdown_arrow">&#9662;</div>
											<select name="upload_wire_six_type" id="" class="form-control">
												<option value="Kanthal" <?php if ($_POST['upload_wire_six_type'] == 'Kanthal') echo htmlentities('selected'); ?>>Kanthal</option>
												<option value="N200" <?php if ($_POST['upload_wire_six_type'] == 'N200') echo htmlentities('selected'); ?>>N200</option>
												<option value="N80" <?php if ($_POST['upload_wire_six_type'] == 'N80') echo htmlentities('selected'); ?>>N80</option>
												<option value="Stainless Steel" <?php if ($_POST['upload_wire_six_type'] == 'Stainless Steel') echo htmlentities('selected'); ?>>Stainless Steel</option>
												<option value="Titanium" <?php if ($_POST['upload_wire_six_type'] == 'Titanium') echo htmlentities('selected'); ?>>Titanium</option>
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
				<div class="modal-footer" style="padding:10px 50px">	
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 left-no-padding">
						<h4 class="required_note">* Required Field</h4>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 right-no-padding">
						<button type="button" class="btn btn-success btn-block export_upload" name="upload_submit">Upload Coil</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Error Modal -->
	<div class="modal fade error_modal" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<div class="modal-header" style="padding:10px 50px;">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="error">Error</h4>
				</div>
				<div class="modal-body" style="padding:15px 50px;">
					<?php echo htmlspecialchars($_SESSION['error']); ?>
				</div>
			</div>
		</div>
	</div>

	<!-- Message Modal -->
	<div class="modal fade message_modal" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<div class="modal-header" style="padding:10px 50px;">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4>Success</h4>
				</div>
				<div class="modal-body" style="padding:15px 50px;">
					<?php echo htmlspecialchars($_SESSION['message']); ?>
				</div>
			</div>
		</div>
	</div>

	<!-- Featured Builds -->
	<div id="featured_section">
		<div class="container">
			<h1 class="text-center">Featured Builds</h1>
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 coil">
					<a href="https://coilerz.com/coil.php?coil_id=74etRrBdi6t7FaS1DCPCVVeAbrL6PZatV2gjnfI62oKjTxWCGhvG&page=1">
						<img src="coils/74etRrBdi6t7FaS1DCPCVVeAbrL6PZatV2gjnfI62oKjTxWCGhvG/coil_image.jpeg" class="img-responsive">
						<h3 class="text-center">Twisted stainless dual</h3>
						<p class="text-center">Uploaded By: <a href="account.php?username=Kubbur&page=1" class="user-link">Kubbur</a></p>
					</a>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 coil">
					<a href="https://coilerz.com/coil.php?coil_id=mizu4SkqNvBl0B88np89eGBJ0FGChfG9jTfVOOzJQ5vaIw3mqOn2&page=1">
						<img src="coils/mizu4SkqNvBl0B88np89eGBJ0FGChfG9jTfVOOzJQ5vaIw3mqOn2/coil_image.jpeg" class="img-responsive">
						<h3 class="text-center">22g Mirror Coil</h3>
						<p class="text-center">Uploaded By: <a href="account.php?username=coilerz&page=1" class="user-link">Coilerz</a></p>
					</a>
				</div><div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 coil">
					<a href="https://coilerz.com/coil.php?coil_id=iVfitg2rMpMwcsTcdkbPeW6SNNk45MQontORzanVLrIO0bSJLQ&page=1">
						<img src="coils/iVfitg2rMpMwcsTcdkbPeW6SNNk45MQontORzanVLrIO0bSJLQ/coil_image.jpeg" class="img-responsive">
						<h3 class="text-center">Cloudchaser</h3>
						<p class="text-center">Uploaded By: <a href="account.php?username=datphung116&page=1" class="user-link">datphung116</a></p>
					</a>
				</div><div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 coil">
					<a href="https://coilerz.com/coil.php?coil_id=tRyXOPF8w144Bu2Enww1Ue5XA1AsjEyIEyXKbxeBn2Sd8jxEquO&page=1">
						<img src="coils/tRyXOPF8w144Bu2Enww1Ue5XA1AsjEyIEyXKbxeBn2Sd8jxEquO/coil_image.jpeg" class="img-responsive">
						<h3 class="text-center">26/32 Dual Clapton Build</h3>
						<p class="text-center">Uploaded By: <a href="account.php?username=Joe_Scotto&page=1" class="user-link">Joe_Scotto</a></p>
					</a>
				</div>
			</div>
		</div>
	</div>

	<!-- Most Viewed Builds -->
	<div id="most_viewed_section">
		<div class="container">
			<div class="top">
				<h1>Most Viewed Builds</h1>
				<ul class="nav nav-pills pull-right">
					<li class="active" id="today"><a>Today</a></li>
					<li id="week"><a>This Week</a></li>
					<li id="month"><a>This Month</a></li>
					<li id="time"><a class="last">All time</a></li>
				</ul>
			</div>
			<!-- Today -->
			<div id="most_day">
				<div class="row">
					<?php foreach (Statistics::getTopCoils("today", "views") as $data) : ?>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 coil">
							<a href="coil.php?coil_id=<?php echo htmlentities($data['coil_id']) ?>&page=1">
								<img src="<?php echo htmlentities($data['image']); ?>" class="img-responsive">
								<h3 class="text-center"><?php echo htmlentities($data['coil_name']); ?></h3>
								<?php if ($data['views'] == 0) { ?>
								<p class="text-center"><?php echo htmlspecialchars(number_format($data['views'])); ?> Views</p>
								<?php } else if ($data['views'] == 1) {?>
									<p class="text-center"><?php echo htmlspecialchars(number_format($data['views'])); ?> View</p>
								<?php } else { ?>
									<p class="text-center"><?php echo htmlspecialchars(number_format($data['views'])); ?> Views</p>
								<?php } ?>
							</a>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
			<!-- This Week -->
			<div id="most_week">
				<div class="row">
					<?php foreach (Statistics::getTopCoils("week", "views") as $data) : ?>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 coil">
							<a href="coil.php?coil_id=<?php echo htmlentities($data['coil_id']) ?>&page=1">
								<img src="<?php echo htmlentities($data['image']); ?>" class="img-responsive">
								<h3 class="text-center"><?php echo htmlentities($data['coil_name']); ?></h3>
								<?php if ($data['views'] == 0) { ?>
								<p class="text-center"><?php echo htmlspecialchars(number_format($data['views'])); ?> Views</p>
								<?php } else if ($data['views'] == 1) {?>
									<p class="text-center"><?php echo htmlspecialchars(number_format($data['views'])); ?> View</p>
								<?php } else { ?>
									<p class="text-center"><?php echo htmlspecialchars(number_format($data['views'])); ?> Views</p>
								<?php } ?>
							</a>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
			<!-- This Month -->
			<div id="most_month">
				<div class="row">
					<?php foreach (Statistics::getTopCoils("month", "views") as $data) : ?>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 coil">
							<a href="coil.php?coil_id=<?php echo htmlentities($data['coil_id']) ?>&page=1">
								<img src="<?php echo htmlentities($data['image']); ?>" class="img-responsive">
								<h3 class="text-center"><?php echo htmlentities($data['coil_name']); ?></h3>
								<?php if ($data['views'] == 0) { ?>
								<p class="text-center"><?php echo htmlspecialchars(number_format($data['views'])); ?> Views</p>
								<?php } else if ($data['views'] == 1) {?>
									<p class="text-center"><?php echo htmlspecialchars(number_format($data['views'])); ?> View</p>
								<?php } else { ?>
									<p class="text-center"><?php echo htmlspecialchars(number_format($data['views'])); ?> Views</p>
								<?php } ?>
							</a>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
			<!-- All Time -->
			<div id="most_time">
				<div class="row">
					<?php foreach (Statistics::getTopCoils("allTime", "views") as $data) : ?>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 coil">
							<a href="coil.php?coil_id=<?php echo htmlentities($data['coil_id']) ?>&page=1">
								<img src="<?php echo htmlentities($data['image']); ?>" class="img-responsive">
								<h3 class="text-center"><?php echo htmlentities($data['coil_name']); ?></h3>
								<?php if ($data['views'] == 0) { ?>
								<p class="text-center"><?php echo htmlspecialchars(number_format($data['views'])); ?> Views</p>
								<?php } else if ($data['views'] == 1) {?>
									<p class="text-center"><?php echo htmlspecialchars(number_format($data['views'])); ?> View</p>
								<?php } else { ?>
									<p class="text-center"><?php echo htmlspecialchars(number_format($data['views'])); ?> Views</p>
								<?php } ?>
							</a>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Highest Rated Builds -->
	<div id="highest_rated_section">
		<div class="container">
			<div class="top">
				<h1>Highest Rated Builds</h1>
				<ul class="nav nav-pills pull-right">
					<li class="active" id="today"><a>Today</a></li>
					<li id="week"><a>This Week</a></li>
					<li id="month"><a>This Month</a></li>
					<li id="time"><a class="last">All time</a></li>
				</ul>
			</div>
			<!-- Today -->
			<div id="this_day">
				<div class="row">
					<?php foreach (Statistics::getTopCoils("today", "likes") as $data) : ?>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 coil">
							<a href="coil.php?coil_id=<?php echo htmlentities($data['coil_id']) ?>&page=1">
								<img src="<?php echo htmlentities($data['image']); ?>" class="img-responsive">
								<h3 class="text-center"><?php echo htmlentities($data['coil_name']); ?></h3>
								<?php if ($data['likes'] == 0) { ?>
								<p class="text-center"><?php echo htmlspecialchars(number_format($data['likes'])); ?> Likes</p>
								<?php } else if ($data['likes'] == 1) {?>
									<p class="text-center"><?php echo htmlspecialchars(number_format($data['likes'])); ?> Like</p>
								<?php } else { ?>
									<p class="text-center"><?php echo htmlspecialchars(number_format($data['likes'])); ?> Likes</p>
								<?php } ?>
							</a>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
			<!-- This Week -->
			<div id="this_week">
				<div class="row">
					<?php foreach (Statistics::getTopCoils("week", "likes") as $data) : ?>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 coil">
							<a href="coil.php?coil_id=<?php echo htmlentities($data['coil_id']) ?>&page=1">
								<img src="<?php echo htmlentities($data['image']); ?>" class="img-responsive">
								<h3 class="text-center"><?php echo htmlentities($data['coil_name']); ?></h3>
								<?php if ($data['likes'] == 0) { ?>
								<p class="text-center"><?php echo htmlspecialchars(number_format($data['likes'])); ?> Likes</p>
								<?php } else if ($data['likes'] == 1) {?>
									<p class="text-center"><?php echo htmlspecialchars(number_format($data['likes'])); ?> Like</p>
								<?php } else { ?>
									<p class="text-center"><?php echo htmlspecialchars(number_format($data['likes'])); ?> Likes</p>
								<?php } ?>
							</a>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
			<!-- This Month -->
			<div id="this_month">
				<div class="row">
					<?php foreach (Statistics::getTopCoils("month", "likes") as $data) : ?>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 coil">
							<a href="coil.php?coil_id=<?php echo htmlentities($data['coil_id']) ?>&page=1">
								<img src="<?php echo htmlentities($data['image']); ?>" class="img-responsive">
								<h3 class="text-center"><?php echo htmlentities($data['coil_name']); ?></h3>
								<?php if ($data['likes'] == 0) { ?>
								<p class="text-center"><?php echo htmlspecialchars(number_format($data['likes'])); ?> Likes</p>
								<?php } else if ($data['likes'] == 1) {?>
									<p class="text-center"><?php echo htmlspecialchars(number_format($data['likes'])); ?> Like</p>
								<?php } else { ?>
									<p class="text-center"><?php echo htmlspecialchars(number_format($data['likes'])); ?> Likes</p>
								<?php } ?>
							</a>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
			<!-- All Time -->
			<div id="all_time">
				<div class="row">
					<?php foreach (Statistics::getTopCoils("allTime", "likes") as $data) : ?>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 coil">
							<a href="coil.php?coil_id=<?php echo htmlentities($data['coil_id']) ?>&page=1">
								<img src="<?php echo htmlentities($data['image']); ?>" class="img-responsive">
								<h3 class="text-center"><?php echo htmlentities($data['coil_name']); ?></h3>
								<?php if ($data['likes'] == 0) { ?>
								<p class="text-center"><?php echo htmlspecialchars(number_format($data['likes'])); ?> Likes</p>
								<?php } else if ($data['likes'] == 1) {?>
									<p class="text-center"><?php echo htmlspecialchars(number_format($data['likes'])); ?> Like</p>
								<?php } else { ?>
									<p class="text-center"><?php echo htmlspecialchars(number_format($data['likes'])); ?> Likes</p>
								<?php } ?>
							</a>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>

	<!-- Footer -->
	<?php require 'templates/footer.php'; ?>
</body>
</html>

