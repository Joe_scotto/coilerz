<?php

class Statistics {
	/**
	 * Updates hit counter
	 * @param  string $ip Users IP address
	 * @return bool True if success false if failed
	 */
	public function updateHitCounter ($ip) {
		//Database Connection 
		$db = Database::getInstance();

		//Select user from database where ip is found
		$query = $db->getConnection()->prepare("SELECT * FROM `hit_counter` WHERE ip = :ip");
		$query->execute(array(
			':ip' => $ip
		));

		//Return number or rows found
		$rowCount = $query->rowCount();

		//If row is not found, add count
		if ($rowCount == 0) {
			$queryUpdate = $db->getConnection()->prepare("INSERT INTO `hit_counter` (ip, visits) VALUES (:ip, :visits)");
			$queryUpdate->execute(array(
				':ip' => $ip,
				':visits' => 1
			));
		} else if ($rowCount >= 1) {
			$queryUpdate = $db->getConnection()->prepare("UPDATE `hit_counter` SET visits = visits + 1 WHERE ip = :ip");
			$queryUpdate->execute(array(
				':ip' => $ip,
			));
		}
	}

	/**
	 * Grabs top 4 coils from coils database
	 * @param  string $timeFrame How long to go back
	 * @param  string $orderBy What to grab rows by
	 * @return array Contains 4 results
	 */
	public static function getTopCoils ($timeFrame, $orderBy) {
		//Database Connection 
		$db = Database::getInstance();

		//Determine what to return
		if ($timeFrame == "today") {
			$query = $db->getConnection()->prepare("SELECT * FROM `coils` WHERE date_uploaded = :date_uploaded ORDER BY {$orderBy} DESC LIMIT 4");
		} else if ($timeFrame == "week") {
			$query = $db->getConnection()->prepare("SELECT * FROM `coils` WHERE sql_time >= NOW() - INTERVAL 1 WEEK AND sql_time < NOW() ORDER BY {$orderBy} DESC LIMIT 4");
		} else if ($timeFrame == "month") {
			$query = $db->getConnection()->prepare("SELECT * FROM `coils` WHERE sql_time >= NOW() - INTERVAL 1 MONTH AND sql_time < NOW() ORDER BY {$orderBy} DESC LIMIT 4");
		} else if ($timeFrame == "allTime") {
			$query = $db->getConnection()->prepare("SELECT * FROM `coils` ORDER BY {$orderBy} DESC LIMIT 4");
		}

		//Execute Query
		$query->execute(array(
			':date_uploaded' => date("F j, Y", time())
		));

		//Return query results
		return $query;
	}
}