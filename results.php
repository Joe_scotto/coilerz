<?php require 'core/init.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<!-- Bootstrap Mobile Optimization -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<!-- Meta Tags -->
	<title>Coilerz - Search for "<?php echo htmlspecialchars($_GET['search_term']); ?>"</title>
	<!-- Favicon -->
	<link rel="icon" href="images/logo.png">
	<!-- Main Stylesheet -->
	<link rel="stylesheet" href="css/style.css">
	<!-- Bootstrap CDN CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
</head>
<body>
	<!-- Header -->
	<?php require 'templates/header.php'; ?>
	
	<!-- Results Header -->
	<div id="results_header" class="text-center">
		<div class="container">
			<h1><?php echo htmlspecialchars(Results::getNumberOfCoils($_GET['search_term'])); ?></h1>
		</div>
	</div>
	

	<!-- Coils Found -->
	<div id="results_coils">
		<div class="container">
			<div class="row">
				<?php foreach (Results::listFoundCoils($_GET['search_term'], $_GET['page']) as $data) : ?>
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 coil">
						<a href="coil.php?coil_id=<?php echo htmlspecialchars($data['coil_id']); ?>&page=1">
							<img src="coils/<?php echo htmlspecialchars($data['coil_id']); ?>/coil_image.jpeg" class="img-responsive">
							<h3 class="text-center"><?php echo htmlspecialchars($data['coil_name']); ?></h3>
							<?php if ($data['views'] == 0) { ?>
								<p class="text-center"><?php echo htmlspecialchars(number_format($data['views'])); ?> Views</p>
							<?php } else if ($data['views'] == 1) {?>
								<p class="text-center"><?php echo htmlspecialchars(number_format($data['views'])); ?> View</p>
							<?php } else { ?>
								<p class="text-center"><?php echo htmlspecialchars(number_format($data['views'])); ?> Views</p>
							<?php } ?>
						</a>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>

	<!-- Results Pagination -->
	<div id="results_pagination" class="text-center">
		<div class="container">
			<!-- Verify there are coils -->
			<?php if (Results::getNumericalNumberOfCoils($_GET['search_term']) > 12) { ?>
				<div class="pagination">
					<!-- If page is eqaul to first don't display last button -->
					<?php if ($_GET['page'] > 1) { ?>
					<li><a href="results.php?search_term=<?php echo htmlspecialchars($_GET['search_term']); ?>&page=<?php echo htmlspecialchars($_GET['page']) - 1; ?>">Last</a></li>
					<?php } ?>
					<!-- List numerical pages -->
					<?php for($x = 1; $x <= Results::listPagination($_GET['search_term'], $_GET['page']); $x++) : ?>
						<li><a href="results.php?search_term=<?php echo htmlspecialchars($_GET['search_term']); ?>&page=<?php echo htmlspecialchars($x); ?>"<?php if($_GET['page'] == $x) {echo htmlspecialchars("class=selected");}?>><?php echo htmlspecialchars($x); ?></a></li>
					<?php endfor; ?>
					<!-- If page is eqaul to last don't display next button -->
					<?php if ($_GET['page'] < Results::listPagination($_GET['search_term'], $_GET['page'])) { ?>
					<li><a href="results.php?search_term=<?php echo htmlspecialchars($_GET['search_term']); ?>&page=<?php echo htmlspecialchars($_GET['page']) + 1; ?>">Next</a></li>
					<?php } ?>
				</div>
			<?php } ?>
		</div>
	</div>
	
	<!-- Footer -->
	<?php require 'templates/footer.php' ?>
</body>
</html>

