<?php

class Registration {
	/**
	 * Verifies that the username is not taken
	 * @param  mixed $data Post data
	 * @return bool Returns false if username exists, otherwise runs 'registerUser' function
	 */
	public function initiateRegistration ($data, $url) {
		//Database Connection
		$db = Database::getInstance();

		//Query Values
		$username = trim($data['sign_up_username']);

		//Query to see if the username is found
		$query = $db->getConnection()->prepare("SELECT username FROM `users` WHERE username = :username");
		$query->execute(array(
			':username' => $username
		));

		//Number of rows returned
		$rowCount = $query->rowCount();

		//Check number of rows
		if ($rowCount <= 0) {
			//Sleep to prevent spam registration
			sleep(5);

			//Register the user
			$this->registerUser($data, $url);
		} else {
			$_SESSION['sign_up_error'] = "Username is taken";
		}
	}

	/**
	 * Registers the user
	 * @param  mixed $data Post data
	 * @return string Success Message
	 */
	public function registerUser ($data, $url) {
		//Database Connection
		$db = Database::getInstance();

		//Defining values for query
		$email = strtolower(trim($data['sign_up_email']));
		$username = trim($data['sign_up_username']);
		$password = $db->hashPassword($data['sign_up_password']);
		$date_joined = date("F j, Y", time());
		$session_id = password_hash(uniqid(), PASSWORD_BCRYPT);

		if (isset($data['sign_up_newsletter'])) {
			$newsLetter = 1;
		} else {
			$newsLetter = 0;
		}

		//Insert the user
		$query = $db->getConnection()->prepare("INSERT INTO `users` (email, username, password, date_joined, session_id, newsletter) VALUES(:email, :username, :password, :date_joined, :session_id, :newsletter)");
		$query->execute(array(
			':email' => $email,
			':username' => $username,
			':password' => $password,
			':date_joined' => $date_joined,
			':session_id' => $session_id,
			':newsletter' => $newsLetter
		));

		//Make a directory for the user
		mkdir('users/' . $username);
		copy('images/profile_picture.jpg', 'users/' . $username . '/profile_picture.jpg');

		//Return Success Message
		$_SESSION['message'] = "Registration successful, you may now login";
		setcookie('sessionPersist', 1);
		header("Location: " . $url);
	} 

	/**
	 * Initiates User Login
	 * @param  mixed $data Post data
	 * @return mixed Checks if the user wants to be remembered and runs the corresponding function ('loginRemember', 'loginNoRemember')
	 */
	public function initiateLogin ($data, $url) {
		//Database Connection 
		$db = Database::getInstance();

		//Defining values for query
		$username = trim($data['login_username']);

		//Grab password from database based on username
		$query = $db->getConnection()->prepare("SELECT * FROM `users` WHERE username = :username");
		$query->execute(array(
			':username' => $username
		));

		//Query Results
		$queryResults = $query->fetch(PDO::FETCH_ASSOC);

		//Check if the password is correct
		if(password_verify($data['login_password'], $queryResults['password'])) {
			if (isset($data['login_remember_me'])) {
				$this->loginRemember($data, $queryResults, $url);
			} else {
				$this->loginNoRemember($data, $queryResults, $url);
			}
		} else {
			//Pause script if password is incorrect
			sleep(5);
			$_SESSION['login_error'] = "Incorrect username or password";
		}
	}

	/**
	 * Sets persistant cookies to keep user logged in
	 * @param  mixed $data Post data
	 * @return session Returns a cookie
	 */
	public function loginRemember ($data, $queryResults, $url) {
		//Database Connection 
		$db = Database::getInstance();

		$session_id = password_hash(uniqid(), PASSWORD_BCRYPT);
		$token = str_replace('$2y$10$', '', password_hash(uniqid() . uniqid(), PASSWORD_BCRYPT));
		$tokenHash = password_hash($token, PASSWORD_BCRYPT);
		$username = $queryResults['username'];

		$query = $db->getConnection()->prepare("UPDATE `users` SET token = :token WHERE username = :username");
		$query->execute(array(
			':token' => $tokenHash,
			':username' => $username
		));

		//Set session to 'session_id'
		$_SESSION['session_id'] = $queryResults['session_id'];

		//Set cookies for persistant login
		setcookie('username', $queryResults['username'], time() + (10 * 365 * 24 * 60 * 60), '/');
		setcookie("token", $token, time() + (10 * 365 * 24 * 60 * 60), '/');

		//Redirect User
		header("Location: " . $url);
	}

	/**
	 * Sets a session that expires upon browser exit
	 * @param  mixed $data Post data
	 * @return session Returns a session
	 */
	public function loginNoRemember ($data, $queryResults, $url) {
		//Database Connection 
		$db = Database::getInstance();

		//Set session to 'session_id'
		$_SESSION['session_id'] = $queryResults['session_id'];

		//Redirect User
		header("Location: " . $url);
	}

	/**
	 * [checkIfRemembered description]
	 * @param  string $username Username of user attempting to login
	 * @param  strign $token Un-hashed token that is stored in the database
	 * @return session Log the user in if the lookup is correct
	 */
	public function checkIfRemembered ($username, $token, $url) {
		//Database Connection
		$db = Database::getInstance();

		//Run query to select token and session_id from the 'users' table
		$query = $db->getConnection()->prepare("SELECT token, session_id FROM `users` WHERE username = :username");
		$query->execute(array(
			':username' => $username
		));

		//Query Results
		$queryResults = $query->fetch(PDO::FETCH_ASSOC);

		//Script auto errors if you do not perform this check
		if (!empty($username) && !empty($token) && empty($_SESSION['session_id'])) {
			//Check if the token matches the hashed token
			if(password_verify($token, $queryResults['token'])) {
				$_SESSION['session_id'] = $queryResults['session_id'];
			} else {
				//Unset all sessions and cookies here due to failed login
				$this->logout($url);
			}
		}
	}

	/**
	 * Verify the user is logged in
	 * @param  string $session_id The id of the user
	 * @return bool True if logged in, false if not
	 */
	public function checkLogin ($session_id) {
		//Database Connection
		$db = Database::getInstance();

		//First make sure the session is actually set
		if (isset($session_id)) {
			//Select data from database where session_id matches
			$query = $db->getConnection()->prepare("SELECT * FROM `users` WHERE session_id = :session_id");
			$query->execute(array(
				':session_id' => $session_id
			));

			//Return number of rows
			$rowCount = $query->rowCount();

			//Validate that the session is actually a row in the database
			if ($rowCount >= 1) {
				return true;
			} else {
				return false;
			}
		} else if (!isset($session_id)){
			return false;
		}
	}

	/**
	 * Logs the user out and unsets any sessions and/or cookies
	 * @param  string $url url of the current page the user is on
	 * @return header Returns the user to the page that they were on before logout
	 */
	public function logout ($url, $message = null) {
		setcookie('username', null, time() - 1, '/');
		setcookie("token", null, time() - 1, '/');
		session_unset($_SESSION['session_id']);

		if (!empty($message)) {
			$_SESSION['message'] = $message;
			setcookie("sessionPersist", 1);
		}

		header("Location: " . $url);
	}
}