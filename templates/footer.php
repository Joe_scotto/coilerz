<div id="footer">
	<div class="container text-center">
		<div class="row">
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
				<a href="https://www.facebook.com/Coilerz/">
					<img src="images/facebook.png" class="img-responsive">
				</a>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
				<a href="https://twitter.com/coilerz">
					<img src="images/twitter.png" class="img-responsive">
				</a>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
				<a href="http://instagram.com/coilerz">
					<img src="images/instagram.png" class="img-responsive">
				</a>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
				<a href="https://www.youtube.com/channel/UC_T0OfN5BLoUkh0uww2gphQ">
					<img src="images/youtube.png" class="img-responsive">
				</a>
			</div>
			<div class="col-lg-12 text-center bot">©2016 Coilerz - - - <a href="terms.php">Terms and Conditions</a></div>
		</div>
	</div>
</div>

<!-- jQuery CDN -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<!-- CropIt JS -->
<script src="script/cropit.js"></script>
<!-- Validator JS -->
<script src="script/validator.js"></script>
<!-- Bootstrap CDN JS -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<script src="script/main.js"></script>

<!-- Show login modal if there is an error -->
<?php if (isset($_SESSION['login_error'])) {?>
<script>
$(document).ready(function (e) {
	$(".login_modal").modal("show");
});
</script>
<?php } ?>

<!-- Show sign up modal if there is an error -->
<?php if (isset($_SESSION['sign_up_error'])) {?>
<script>
$(document).ready(function (e) {
	$(".sign_up_modal").modal("show");
});
</script>
<?php } ?>

<!-- Show contact modal if there is an error -->
<?php if (isset($_SESSION['contact_error'])) {?>
<script>
$(document).ready(function (e) {
	$(".contact_modal").modal("show");
});
</script>
<?php } ?>

<!-- Show account settings modal if there is an error -->
<?php if (isset($_SESSION['account_settings_error'])) {?>
<script>
$(document).ready(function (e) {
	$(".account_settings_modal").modal("show");
});
</script>
<?php } ?>

<!-- Show search modal if there is an error -->
<?php if (isset($_SESSION['search_error'])) {?>
<script>
$(document).ready(function (e) {
	$(".search_modal").modal("show");
});

</script>
<?php } ?>

<!-- Show upload modal if there is an error -->
<?php if (isset($_SESSION['upload_error'])) {?>
<script>
$(document).ready(function (e) {
	$(".upload_modal").modal("show");
});
</script>
<?php } ?>

<!-- Show error modal if there is an error -->
<?php if (isset($_SESSION['error'])) {?>
<script>
$(document).ready(function (e) {
	$(".error_modal").modal("show");
});
</script>
<?php } ?>

<!-- Show message modal if there is an error -->
<?php if (isset($_SESSION['message'])) {?>
<script>
$(document).ready(function (e) {
	$(".message_modal").modal("show");
});
</script>
<?php } ?>

<!-- Show flag modal if there is an error -->
<?php if (isset($_SESSION['coil_flag_error'])) {?>
<script>
$(document).ready(function (e) {
	$(".flag_modal").modal("show");
});
</script>
<?php } ?>

<!-- Show flag modal if there is an error -->
<?php if (isset($_SESSION['edit_error'])) {?>
<script>
$(document).ready(function (e) {
	$(".edit_modal").modal("show");
});
</script>
<?php } ?>