<?php

class Upload {
	/**
	 * Initiates upload of coil
	 * @param  string $session_id Session identifier for logged in user
	 * @param  mixed $data $_POST data
	 * @return mixed Calls appropriate functions to upload the coil
	 */
	public function initiateUpload ($session_id, $data, $url) {
		//Database Connection
		$db = Database::getInstance();

		//Initiate functions 
		$coil_id = $this->createCoilID();
		$wire = $this->formatWire($data);
		$date_uploaded = date("F j, Y", time());
		$wire_gauges = $this->formatWireGauges($data);
		$wire_types = $this->formatWireTypes($data);

		//Directory of coil
		$directory = 'coils/' . $coil_id;

		//Create directory for coil
		mkdir($directory);

		//Upload image
		$this->uploadImage($data, $directory);

		//Insert Data
		$query = $db->getConnection()->prepare("INSERT INTO `coils` (coil_name, resistance, inner_diameter, wraps, coils, atomizer, difficulty, description, wire, image, username, coil_id, date_uploaded, sql_time, wire_gauges, wire_types) VALUES(:coil_name, :resistance, :inner_diameter, :wraps, :coils, :atomizer, :difficulty, :description, :wire, :image, :username, :coil_id, :date_uploaded, now(), :wire_gauges, :wire_types)");
		$query->execute(array(
			':coil_name' => $data['upload_coil_name'],
			':resistance' => $data['upload_resistance'] + 0,
			':inner_diameter' => $data['upload_inner_diameter'] + 0 . $data['upload_inner_diameter_unit'],
			':wraps' => $data['upload_wraps'] + 0,
			':coils' => $data['upload_number_of_coils'],
			':atomizer' => ($data['upload_atomizer'] !== "" ? $data['upload_atomizer'] : "Not Specified"),
			':difficulty' => $this->checkDifficulty($_POST),
			':description' => $data['upload_description'],
			':wire' => $wire,
			':image' => $directory . '/coil_image.jpeg',
			':username' => Account::getUserPersonalInfo($session_id)['username'],
			':coil_id' => $coil_id,
			':date_uploaded' => $date_uploaded,
			':wire_gauges' => $wire_gauges,
			':wire_types' => $wire_types
		));

		//Update users coils
		$queryUpdateUser = $db->getConnection()->prepare("UPDATE `users` SET coils = coils + 1, last_upload = NOW() WHERE username = :username");
		$queryUpdateUser->execute(array(
			':username' => Account::getUserPersonalInfo($session_id)['username']
		));

		//Return message to user notifying them of a successfull upload
		$_SESSION['message'] = "Congratulations! Your coil was uploaded successfully!";
		setcookie("sessionPersist", 1);
		header("Location: " . $url);
	}

	/**
	 * Moves image to coils folder
	 * @param  string $session_id Session identifier for logged in user
	 * @param  mixed $data $_POST data
	 * @return bool True is image is uploaded, false if not.
	 */
	public function uploadImage ($data, $directory) {
		//Database Connection 
		$db = Database::getInstance();

		//Define variables
		$base64 = $data['upload_image'];
		$directory = $directory . '/coil_image.jpeg';

		//Decode base64 into image
		$db->decodeBase64($base64, $directory);
	}

	/**
	 * @param  string $string What is getting hashed
	 * @return string Returns a hashed string
	 */
	public function createCoilID () {
		//Create random ID
		$hash = password_hash(uniqid() . uniqid() . rand(), PASSWORD_BCRYPT, array(
			'cost' => 10
		));
		
		//Return ID
		return str_replace(array("$2y$10$", ".", "/", "-", ","), "", $hash);
	}

	/**
	 * Prepares the raw data for the database.
	 * @param  mixed $data $_POST data
	 * @return string Formatted wires ready for the database
	 */
	public function formatWire ($data) {
		//Wire One
		if (!empty($data['upload_wire_one_gauge']) && !empty($data['upload_wire_one_type'])) {
 			$wire = $data['upload_wire_one_gauge'] . "g " . $data['upload_wire_one_type'];
		}

		//Wire Two
		if (!empty($data['upload_wire_two_gauge']) && !empty($data['upload_wire_two_type'])) {
 			$wire .= " - " . $data['upload_wire_two_gauge'] . "g " . $data['upload_wire_two_type'];
		}

		//Wire Three
		if (!empty($data['upload_wire_three_gauge']) && !empty($data['upload_wire_three_type'])) {
 			$wire .= " - " . $data['upload_wire_three_gauge'] . "g " . $data['upload_wire_three_type'];
		}

		//Wire Four
		if (!empty($data['upload_wire_four_gauge']) && !empty($data['upload_wire_four_type'])) {
 			$wire .= " - " . $data['upload_wire_four_gauge'] . "g " . $data['upload_wire_four_type'];
		}

		//Wire Five
		if (!empty($data['upload_wire_five_gauge']) && !empty($data['upload_wire_five_type'])) {
 			$wire .= " - " . $data['upload_wire_five_gauge'] . "g " . $data['upload_wire_five_type'];
		}

		//Wire Six		
		if (!empty($data['upload_wire_six_gauge']) && !empty($data['upload_wire_six_type'])) {
 			$wire .= " - " . $data['upload_wire_six_gauge'] . "g " . $data['upload_wire_six_type'];
		}

		//Returns string with first letter in each word uppercase
		return ucwords($wire);
	}

	/**
	 * Prepares the raw data for the database.
	 * @param  mixed $data $_POST data
	 * @return string Formatted wire gauges ready for the database
	 */
	public function formatWireGauges ($data) {
		//Wire One
		if (!empty($data['upload_wire_one_gauge']) && !empty($data['upload_wire_one_type'])) {
 			$wire = $data['upload_wire_one_gauge'];
		}

		//Wire Two
		if (!empty($data['upload_wire_two_gauge']) && !empty($data['upload_wire_two_type'])) {
 			$wire .= "," . $data['upload_wire_two_gauge'];
		}

		//Wire Three
		if (!empty($data['upload_wire_three_gauge']) && !empty($data['upload_wire_three_type'])) {
 			$wire .= "," . $data['upload_wire_three_gauge'];
		}

		//Wire Four
		if (!empty($data['upload_wire_four_gauge']) && !empty($data['upload_wire_four_type'])) {
 			$wire .= "," . $data['upload_wire_four_gauge'];
		}

		//Wire Five
		if (!empty($data['upload_wire_five_gauge']) && !empty($data['upload_wire_five_type'])) {
 			$wire .= "," . $data['upload_wire_five_gauge'];
		}

		//Wire Six		
		if (!empty($data['upload_wire_six_gauge']) && !empty($data['upload_wire_six_type'])) {
 			$wire .= "," . $data['upload_wire_six_gauge'];
		}

		//Returns string with first letter in each word uppercase
		return ucwords($wire);
	}

	/**
	 * Prepares the raw data for the database.
	 * @param  mixed $data $_POST data
	 * @return string Formatted wire types ready for the database
	 */
	public function formatWireTypes ($data) {
		//Wire One
		if (!empty($data['upload_wire_one_gauge']) && !empty($data['upload_wire_one_type'])) {
 			$wire = $data['upload_wire_one_type'];
		}

		//Wire Two
		if (!empty($data['upload_wire_two_gauge']) && !empty($data['upload_wire_two_type'])) {
 			$wire .= "," . $data['upload_wire_two_type'];
		}

		//Wire Three
		if (!empty($data['upload_wire_three_gauge']) && !empty($data['upload_wire_three_type'])) {
 			$wire .= "," . $data['upload_wire_three_type'];
		}

		//Wire Four
		if (!empty($data['upload_wire_four_gauge']) && !empty($data['upload_wire_four_type'])) {
 			$wire .= "," . $data['upload_wire_four_type'];
		}

		//Wire Five
		if (!empty($data['upload_wire_five_gauge']) && !empty($data['upload_wire_five_type'])) {
 			$wire .= "," . $data['upload_wire_five_type'];
		}

		//Wire Six		
		if (!empty($data['upload_wire_six_gauge']) && !empty($data['upload_wire_six_type'])) {
 			$wire .= "," . $data['upload_wire_six_type'];
		}

		//Returns string with first letter in each word uppercase
		return ucwords($wire);
	}

	/**
	 * Verify user has not uploaded in past 5 minutes
	 * @param  string $session_id ID of the user
	 * @return int 1 if found, 0 if not
	 */
	public static function checkUpload ($session_id) {
		//Database Connection
		$db = Database::getInstance();

		//Grab rows
		$query = $db->getConnection()->prepare("SELECT id FROM `users` WHERE last_upload >= NOW() - INTERVAL 5 MINUTE AND last_upload < NOW() AND username = :username");
		$query->execute(array(
			':username' => Account::getUserPersonalInfo($session_id)['username']
		));

		return $query->rowCount();
	}

	/**
	 * Verifies that the user did not modify the value of difficulty option, sends formatted version
	 * @param  mixed $data $_POST Data
	 * @return string Formatted version of difficulty
	 */
	public static function checkDifficulty ($data) {
		switch($data['upload_difficulty']) {
			case "easy" : $return = "Easy"; 
			break;

			case "medium" : $return = "Medium";
			break;

			case "hard" : $return = "Hard";
			break;

			case "experimental" : $return = "Experimental";
			break;

			default : $return = "Error";
		}

		return $return;
	}

	/**
	 * Verifies that the user did not modify the value of inner diameter option, sends formatted version
	 * @param  mixed $data $_POST Data
	 * @return string Formatted version of difficulty
	 */
	public static function checkInnerDiameter ($data) {
		switch($data['upload_inner_diameter_unit']) {
			case "mm" : $return = "mm"; 
			break;

			case "in" : $return = "in";
			break;

			default : $return = "Error";
		}

		return $return;
	}

	/**
	 * Verifies that the user did not modify the value of wire option, sends formatted version
	 * @param  mixed $data $_POST Data
	 * @return string Formatted version of difficulty
	 */
	public static function checkWireType ($data) {
		switch($data) {
			case "Kanthal" : $return = "Kanthal"; 
			break;

			case "N200" : $return = "N200";
			break;

			case "N80" : $return = "N80";
			break;

			case "Stainless Steel" : $return = "Stainless Steel";
			break;

			case "Titanium" : $return = "Titanium";
			break;

			default : $return = "Error";
		}

		return $return;
	}
}