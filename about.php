<?php require 'core/init.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<!-- Bootstrap Mobile Optimization -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<!-- Meta Tags -->
	<title>Coilerz - About</title>
	<!-- Favicon -->
	<link rel="icon" href="images/logo.png">
	<!-- Main Stylesheet -->
	<link rel="stylesheet" href="css/style.css">
	<!-- Bootstrap CDN CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
</head>
<body>
	<!-- Header -->
	<?php require 'templates/header.php'; ?>

	<!-- Main Content -->
	<div class="container about_container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<img src="images/logo.png" class="img-responsive">
			</div>
			<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
			<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 text-center">
				<p>Coilerz is a website launched by <a href="https://twitter.com/joe_scotto">Joe Scotto</a> on January 1, 2016. The original idea was a simple three page website that would allow vapers to share their build and find builds submitted by other people. The original site was live for a couple months until it was swapped out for the version you’re looking at now. The current version is a massive improvement over the original and added many new features such as coil ratings, views, and comments. Coilerz is constantly growing and gaining new features. We hope that having this site out there helps people enjoy vaping even more. Thank you for using Coilerz!</p>
			</div>
			<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
		</div>
	</div>

	<!-- Footer -->
	<?php require 'templates/footer.php' ?>
</body>
</html>
