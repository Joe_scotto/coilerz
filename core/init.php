<?php

//Start session handling
session_start();

//Google Anylytics
include_once("templates/analyticstracking.php");

//Unset error sessions
unset($_SESSION['login_error']);
unset($_SESSION['sign_up_error']);
unset($_SESSION['contact_error']);
unset($_SESSION['account_settings_error']);
unset($_SESSION['search_error']);
unset($_SESSION['upload_error']);
unset($_SESSION['error']);
unset($_SESSION['coil_flag_error']);
unset($_SESSION['edit_error']);

if (isset($_COOKIE['sessionPersist'])) {
	if ($_COOKIE['sessionPersist'] == 1) {
		//Prepare cookie to unset on next reload
		setcookie('sessionPersist', $_COOKIE['sessionPersist'] + 1);
	} else if ($_COOKIE['sessionPersist'] == 2) {
		unset($_SESSION['message']);

		//Unset Session Persistence Cookie
		setcookie('sessionPersist', 0);
	} else if ($_COOKIE['sessionPersist'] == 0) {
		//Unset Session Persistence Cookie
		setcookie('sessionPersist', "", time()-1);
	}
} else if (!isset($_COOKIE['sessionPersist']) || $_COOKIE['sessionPersist'] == 0) {
	unset($_SESSION['message']);

	//Unset Session Persistence Cookie
	unset($_COOKIE['sessionPersist']);
}

//Testing Error Reporting
error_reporting(0);

//Live Error Reporting
//error_reporting(0);

//Autoload Classes
spl_autoload_register(function ($class) {
	require 'classes/' . $class . '.php';
});

//Configuration
$GLOBALS['config'] = array(
	'mysql' => array(
		'user' => '',
		'pass' => '',
		'host' => '',
		'db' => ''
	)
);

//Initiate Classes 
$mail = new Mail();
$registration = new Registration();
$account = new Account();
$upload = new Upload();
$coil = new Coil();
$statistics = new Statistics();
$database = new Database();
$results = new Results();

//Update hit counter
$statistics->updateHitCounter($_SERVER['REMOTE_ADDR']);

//Display Information Variables
$coilInfo = $coil->getCoilInfo($_GET['coil_id']);

//Update Coil Views
if($_GET['coil_id'] !== null) {
	$coil->updateViews($_GET['coil_id'], $_SERVER['REMOTE_ADDR']);
} 

//Check if liked or disliked
$coil->checkIfLikedOrDisliked($_SESSION['session_id'], $_GET['coil_id']);

//Update Likes 
if (isset($_POST['like'])) {
	if (isset($_SESSION['session_id'])) {
		//$coil->like($_SESSION['session_id'], $_GET['coil_id'], $url);
		$coil->initiateRating($_SESSION['session_id'], $_GET['coil_id'], "like", $url);
	} else {
		$_SESSION['error'] = "You must be logged in to rate coils";
	}
}

//Update Dislikes 
if (isset($_POST['dislike'])) {
	if (isset($_SESSION['session_id'])) {
		//$coil->dislike($_SESSION['session_id'], $_GET['coil_id'], $url);
		$coil->initiateRating($_SESSION['session_id'], $_GET['coil_id'], "dislike", $url);
	} else {
		$_SESSION['error'] = "You must be logged in to rate coils";
	}
}

//Static variables
$url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

//Initiate Search
if (isset($_POST['search_submit'])) {
	if (!empty($_POST['search_term'])) {
		header("Location: results.php?search_term=" . htmlentities(trim($_POST['search_term'])) . "&page=1");
	} else {
		$_SESSION['search_error'] = "You cannot search for nothing";
	}
}

//Iniitiate Contact
if (isset($_POST['contact_submit'])) {
	if (!empty($_POST['contact_name']) && !empty($_POST['contact_subject']) && !empty($_POST['contact_email']) && !empty($_POST['contact_message'])) {
		$mail->sendMessage($_POST, $url);
	} else {
		$_SESSION['contact_error'] = "Please fill in all fields";
	}
} 

//Initiate Registration
if (isset($_POST['sign_up_submit'])) {
	//Check if all inputs are not empty
	if (!empty($_POST['sign_up_email']) && !empty($_POST['sign_up_username']) && !empty($_POST['sign_up_password']) && !empty($_POST['sign_up_confirm_password'])) {
		//Make sure the user accepted the terms and conditions
		if (isset($_POST['sign_up_terms'])) {
			//Make sure passwords match 
			if($_POST['sign_up_password'] == $_POST['sign_up_confirm_password']) {
				//Make sure password is greater than or equal to 6 characters
				if (strlen($_POST['sign_up_password']) >= 6) {
					//Make sure username is less than or equal to 18 characters
					if (strlen($_POST['sign_up_username']) <= 18) {
						//Make sure username is greater than 3 characters
						if (strlen($_POST['sign_up_username']) >= 3) {
							if (preg_match('/^[\w-]+$/', $_POST['sign_up_username'])) {
								$registration->initiateRegistration($_POST, $url);
							} else {
								$_SESSION['sign_up_error'] = "Usernames cannot contain special characters";
							}
						} else {
							$_SESSION['sign_up_error'] = "Username must be three or more characters";
						}
					} else {
						$_SESSION['sign_up_error'] = "Username cannot be longer than 18 characters";
					}
				} else {
					$_SESSION['sign_up_error'] = "Password must be six or more characters";
				}
			} else {
				$_SESSION['sign_up_error'] = "Passwords must match";
			}
		} else {
			$_SESSION['sign_up_error'] = "You must accept the terms";
		}
	} else {
		$_SESSION['sign_up_error'] = "Please fill in all fields";
	}
}

//Initiate Login
if (isset($_POST['login_submit'])) {
	if (!empty($_POST['login_username']) && !empty($_POST['login_password'])) {
		$registration->initiateLogin($_POST, $url);
	} else {
		$_SESSION['login_error'] = "Incorrect username or password";
	}
}

//Initiate Account Settings 
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	if ($_SESSION['session_id']) {
		if ($_POST['account_settings_form_identifier'] == "account_settings") {
			if (!empty($_POST['account_settings_profile_picture']) || !empty($_POST['account_settings_password']) || !empty($_POST['account_settings_confirm_password']) || $_POST['account_settings_newsletter'] !== Account::getUserPersonalInfo($_SESSION['session_id'])['newsletter'] || $_POST['account_settings_email'] !== Account::getUserPersonalInfo($_SESSION['session_id'])['email']) {
				//Profile Picture
				if (!empty($_POST['account_settings_profile_picture'])) {
					//Check that image is smaller than 2mb
					if ($_FILES['account_settings_profile_picture_file']['size'] <= 2097152) {
						//Verify image dimensions
						if ($database->checkImageDimensions($_POST['account_settings_profile_picture'], 768, 768)) {
							$account->accountSettingsUpdateProfilePicture($_SESSION['session_id'], $_POST);
						} else {
							$_SESSION['account_settings_error'] = "Please choose a different image";	
						}
					} else {
						$_SESSION['account_settings_error'] = "Image must be smaller than 2mb";
					}
				}

				//Email
				if (!empty($_POST['account_settings_email']) && $_POST['account_settings_email'] !== $account->getUserPersonalInfo($_SESSION['session_id'])['email']) {
					//Verify user gave current password
					if (!empty($_POST['account_settings_current_password'])) {
						$account->accountSettingsUpdateEmail($_SESSION['session_id'], $_POST);
					} else {
						$_SESSION['account_settings_error'] = "Current password required";
					}
				}

				//Password
				if (!empty($_POST['account_settings_password']) && !empty($_POST['account_settings_confirm_password'])) {
					//Verify user gave current password
					if(!empty($_POST['account_settings_current_password'])) {
						//Verify passwords match
						if ($_POST['account_settings_password'] == $_POST['account_settings_confirm_password']) {
							//Verify password is greater than or equal to 6 characters
							if(strlen($_POST['account_settings_password']) >= 6) {
								$account->accountSettingsUpdatePassword($_SESSION['session_id'], $_POST, $url);
							} else {
								$_SESSION['account_settings_error'] = "Password must be six or more characters";
							}
						} else {
							$_SESSION['account_settings_error'] = "Passwords must match";
						}
					} else {
						$_SESSION['account_settings_error'] = "Current password required";
					}
				}

				//NewsLetter 
				if (isset($_POST['account_settings_newsletter'])) {
					$account->accountSettingsUpdateNewsletter($_SESSION['session_id'], 1, $url);
				} else if (!isset($_POST['account_settings_newsletter'])) {
					$account->accountSettingsUpdateNewsletter($_SESSION['session_id'], 0, $url);
				}
			}
		}
	}
}

//Initiate Logout 
if (isset($_POST['logout'])) {
	$registration->logout($url);
}

//Initiate Coil Upload
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	if (isset($_SESSION['session_id'])) {
		if ($_POST['upload_form_identifier'] == "upload_form") {
			//Verify all required sections are not empty
			if (!empty($_POST['upload_coil_name']) && !empty($_POST['upload_resistance']) && !empty($_POST['upload_inner_diameter']) && !empty($_POST['upload_inner_diameter_unit']) && !empty($_POST['upload_wraps']) && !empty($_POST['upload_description']) && !empty($_POST['upload_wire_one_gauge']) && !empty($_POST['upload_wire_one_type']) && !empty($_POST['upload_number_of_coils'])) {
				//Verify user selected a difficulty
				if (isset($_POST['upload_difficulty'])) {
					//Verify user did not modify difficulty
					if (Upload::checkDifficulty($_POST) !== "Error") {
						//Verify user did not modify inner diameter
						if (Upload::checkInnerDiameter($_POST) !== "Error") {
							//Verify user did not modify wire type
							if (Upload::checkWireType($_POST['upload_wire_one_type']) !== "Error" && Upload::checkWireType($_POST['upload_wire_two_type']) !== "Error" && Upload::checkWireType($_POST['upload_wire_three_type']) !== "Error" && Upload::checkWireType($_POST['upload_wire_four_type']) !== "Error" && Upload::checkWireType($_POST['upload_wire_five_type']) !== "Error" && Upload::checkWireType($_POST['upload_wire_six_type']) !== "Error") {
								//Verify Coil name is greater than 6 Characters
								if (strlen($_POST['upload_coil_name']) >= 6) {
									//Verify description is larger than 10
									if (strlen($_POST['upload_description']) >= 10) {
										//Verify user selected an image
										if (!empty($_POST['upload_image']) > 0) {
											//Verify image size is smaller than 5mb
											if ($_FILES['upload_file']['size'] <= 2097152) {
												//Verify the image is correct dimensions
												if ($database->checkImageDimensions($_POST['upload_image'], 768, 768)) {
													//Verify user did not modify min/max lengths
													if (strlen($_POST['upload_coil_name']) <= 32 && strlen($_POST['description']) <= 256) {
														//Check if atomizer is less than 32
														if (strlen($_POST['upload_atomizer']) <= 32) {
															//Check if user has uploaded before
															if (Upload::checkUpload($_SESSION['session_id']) == 0) { 
																$upload->initiateUpload($_SESSION['session_id'], $_POST, $url);
															} else {
																$_SESSION['upload_error'] = "Please wait a bit before uploading again";
															}
														} else {
															$_SESSION['upload_error'] = "Atomizer must be 32 characters or less";
														}
													}
												} else {
													$_SESSION['upload_error'] = "Please choose a different image";
												}
											} else {
												$_SESSION['upload_error'] = "Image must be smaller than 2mb";
											}
										} else {
											$_SESSION['upload_error'] = "Please select an image";
										}
									} else {
										$_SESSION['upload_error'] = "Description must be greater than 10 characters";
									}
								} else {
									$_SESSION['upload_error'] = "Coil name must be six or more characters";
								}
							} else {
								$_SESSION['upload_error'] = "Error";
							}
						} else {
							$_SESSION['upload_error'] = "Error";
						}
					} else {
						$_SESSION['upload_error'] = "Error";
					}
				} else {
					$_SESSION['upload_error'] = "Please choose a difficulty";
				}
			} else {
				$_SESSION['upload_error'] = "Please fill in required (*) fields";
			}
		}
	} 
}

//Initiate Coil Edit
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	if (isset($_SESSION['session_id']) && Coil::checkCoilOwner($_SESSION['session_id'], $_GET['coil_id'])) {
		if ($_POST['edit_form_identifier'] == "edit_form") {
			//Verify all required sections are not empty
			if (!empty($_POST['edit_coil_name']) && !empty($_POST['edit_resistance']) && !empty($_POST['edit_inner_diameter']) && !empty($_POST['edit_inner_diameter_unit']) && !empty($_POST['edit_wraps']) && !empty($_POST['edit_description']) && !empty($_POST['edit_wire_one_gauge']) && !empty($_POST['edit_wire_one_type']) && !empty($_POST['edit_number_of_coils'])) {
				//Verify user selected a difficulty
				if (isset($_POST['edit_difficulty'])) {
					//Verify user did not modify difficulty
					if (Coil::checkDifficulty($_POST) !== "Error") {
						//Verify user did not modify inner diameter
						if (Coil::checkInnerDiameter($_POST) !== "Error") {
							//Verify user did not modify wire type
							if (Coil::checkWireType($_POST['edit_wire_one_type']) !== "Error" && Coil::checkWireType($_POST['edit_wire_two_type']) !== "Error" && Coil::checkWireType($_POST['edit_wire_three_type']) !== "Error" && Coil::checkWireType($_POST['edit_wire_four_type']) !== "Error" && Coil::checkWireType($_POST['edit_wire_five_type']) !== "Error" && Coil::checkWireType($_POST['edit_wire_six_type']) !== "Error") {
								//Verify Coil name is greater than 6 Characters
								if (strlen($_POST['edit_coil_name']) >= 6) {
									//Verify description is larger than 10
									if (strlen($_POST['edit_description']) >= 10) {
										//Verify image size is smaller than 2mb
										if ($_FILES['edit_file']['size'] <= 2097152) {
											//Verify the image is correct dimensions
											if ($database->checkImageDimensions($_POST['edit_image'], 768, 768)) {
												//Verify user did not modify min/max lengths
												if (strlen($_POST['edit_coil_name']) <= 32 && strlen($_POST['description']) <= 256) {
													//Check if atomizer is less than 32
													if (strlen($_POST['edit_atomizer']) <= 32) {
														$coil->initiateEdit($_POST, $_SESSION['session_id'], $_GET['coil_id'], $url);
													} else {
														$_SESSION['edit_error'] = "Atomizer must be 32 characters or less";
													}
												}
											} else {
												$_SESSION['edit_error'] = "Please choose a different image";
											}
										} else {
											$_SESSION['edit_error'] = "Image must be smaller than 2mb";
										}	
									} else {
										$_SESSION['edit_error'] = "Description must be greater than 10 characters";
									}
								} else {
									$_SESSION['edit_error'] = "Coil name must be six or more characters";
								}
							} else {
								$_SESSION['edit_error'] = "Error";
							}
						} else {
							$_SESSION['edit_error'] = "Error";
						}
					} else {
						$_SESSION['edit_error'] = "Error";
					}
				} else {
					$_SESSION['edit_error'] = "Please choose a difficulty";
				}
			} else {
				$_SESSION['edit_error'] = "Required fields cannot be empty";
			}
		}
	} else {
		$_SESSION['edit_error'] = "Unknown Error, Please try again later";
	}
}

//Check if user is remembered
if (!isset($_SESSION['logout'])) {
	$registration->checkIfRemembered($_COOKIE['username'], $_COOKIE['token'], $url);
} 

//Initiate Comment
if ($_POST['coil_comment_submit']) {
	//Verify user is logged in
	if ($_SESSION['session_id']) {
		//Verify user input a comment
		if (!empty($_POST['coil_comment_comment'])) {
			//Make sure comment is less that 512 characters
			if(strlen($_POST['coil_comment_comment']) <= 512) {
				$coil->addComment($_SESSION['session_id'], $_GET['coil_id'], $_POST, $url);	
			} else {
				$_SESSION['error'] = "Your comment must be shorter than 512 characters";
			}
		} else {
			$_SESSION['error'] = "Your comment cannot be empty";
		}
	} else {
		$_SESSION['error'] = "You must be logged in to comment";
	}
}

//Initiate Coil Flag 
if (isset($_POST['coil_flag_submit'])) {
	if (!empty($_POST['coil_flag_reason'])) {
		Mail::flagCoil($_POST, $_GET['coil_id'], $url);
	} else {
		$_SESSION['coil_flag_error'] = "Please give us a reason";
	}
}

//Delete Coil 
if (isset($_POST['confirm_deletion'])) {
	$coil->deleteCoil($_SESSION['session_id'], $_GET['coil_id'], $url);
}

//Verify the user is logged out
$registration->checkLogin($_SESSION['session_id'], $url);