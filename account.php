<?php require 'core/init.php'; ?>
<?php Account::checkIfUserExists($_GET['username']); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<!-- Bootstrap Mobile Optimization -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<!-- Meta Tags -->
	<title>Coilerz - <?php echo htmlspecialchars($account->getUserPublicInfo($_GET['username'])['username']); ?></title>
	<!-- Favicon -->
	<link rel="icon" href="images/logo.png">
	<!-- Main Stylesheet -->
	<link rel="stylesheet" href="css/style.css">
	<!-- Bootstrap CDN CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
</head>
<body>
	<!-- Header -->
	<?php require 'templates/header.php'; ?>

	<!-- Settings Modal -->
	<div id="settings_modal">
	    <!-- If page belongs to the logged in user -->
		<?php if($registration->checkLogin($_SESSION['session_id']) && $account->getUserPublicInfo($_GET['username'])['username'] == $account->getUserPersonalInfo($_SESSION['session_id'])['username']) { ?>
		<div class="modal fade account_settings_modal" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-md">
				<div class="modal-content">
					<div class="modal-header" style="padding:10px 50px;">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<?php if (isset($_SESSION['account_settings_error'])) { ?>
						<h4 class="error"><?php echo htmlspecialchars($_SESSION['account_settings_error']);?></h4>
						<?php } else { ?>
						<h4>Account Settings</h4>
						<?php }?>
					</div>
					<form action="" method="post" name="account_settings_form" data-toggle="validator" enctype="multipart/form-data">
						<!-- Hidden Identifier -->
						<input type="text" name="account_settings_form_identifier" id="account_settings_form_identifier" value="<?php echo htmlspecialchars($_POST['account_settings_form_identifier']); ?>">
						<div class="modal-body" style="padding:15px 50px;">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="form-group">
									<label for="wraps">Profile Picture</label>
									<label for="cropit-image-input-account-settings" id="select_image">Select Image</label>
									<hr id="upload_divider">
									<div class="image-editor-account-settings">
										<input type="file" class="cropit-image-input" id="cropit-image-input-account-settings" name="account_settings_profile_picture_file" accept="jpg, jpeg, png">
										<div class="cropit-preview"></div>
										<input type="text" id="hidden_base64_account_settings" name="account_settings_profile_picture">
										<input type="range" class="cropit-image-zoom-input range">
									</div>
									<hr>
								</div>
							</div>
							<div class="form-group">
								<label for="account_settings_email">Email</label>
								<input type="email" class="form-control" id="account_settings_email" placeholder="Email" name="account_settings_email" data-error="Please enter a valid email" value="<?php echo htmlspecialchars($account->getUserPersonalInfo($_SESSION['session_id'])['email']); ?>">
								<div class="help-block with-errors"></div>
							</div>
							<div class="form-group">
								<label for="account_settings_password">New Password</label>
								<input type="password" class="form-control" id="account_settings_password" placeholder="Password" name="account_settings_password">
							</div>	
							<div class="form-group">
								<label for="account_settings_confirm_password">Confirm New Password</label>
								<input type="password" class="form-control" id="account_settings_confirm_password" placeholder="Confirm New Password" name="account_settings_confirm_password" data-error="Whoa, these don't match" data-match="#account_settings_password">
								<div class="help-block with-errors"></div>
							</div>	
							<div class="form-group">
								<label for="account_settings_current_password">Current Password</label>
								<input type="password" class="form-control" id="account_settings_current_password" placeholder="Current Password" name="account_settings_current_password" required data-error="Current password is required">
								<div class="help-block with-errors"></div>
							</div>	
							<div class="checkbox" id="account_settings_newsletter_container">
								<label for="account_settings_newsletter">
									<?php if (Account::getUserPersonalInfo($_SESSION['session_id'])['newsletter'] == 1) { ?>
										<input type="checkbox" name="account_settings_newsletter[]" id="account_settings_newsletter" checked value="1">
									<?php } else { ?>
										<input type="checkbox" name="account_settings_newsletter[]" id="account_settings_newsletter" value="0">
									<?php } ?>
									Newsletter
								</label>		
							</div>	
						</div>
						<div class="modal-footer" style="padding:10px 50px">	
							<button type="button" class="btn btn-success btn-block export_account_settings" name="account_settings_submit">Save</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>

	<!-- Profile Picture -->
	<div id="account_profile_picture">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-3 col-xs-3"></div>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
					<img src="users/<?php echo htmlspecialchars($account->getUserPublicInfo($_GET['username'])['username']); ?>/profile_picture.jpg" class="img-responsive img-circle img-center">
				</div>
				<div class="col-lg-4 col-md-4 col-sm-3 col-xs-3"></div>
				</div>
			</div>
		</div>
	</div>

	<!-- User Information -->
	<div id="account_user_info">
		<div class="container">
			<div class="row text-center">
				<!-- If page belongs to the logged in user -->
				<?php if($registration->checkLogin($_SESSION['session_id']) && $account->getUserPublicInfo($_GET['username'])['username'] == $account->getUserPersonalInfo($_SESSION['session_id'])['username']) { ?>
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
						<?php echo htmlspecialchars($account->getUserPublicInfo($_GET['username'])['username']); ?>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
						<a class="contact_button" data-toggle="modal" data-target=".account_settings_modal">Settings</a>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
						<?php if ($account->getUserPublicInfo($_GET['username'])['coils'] == 0) { ?>
							<?php echo htmlspecialchars($account->getUserPublicInfo($_GET['username'])['coils']); ?> Coils
						<?php } else if ($account->getUserPublicInfo($_GET['username'])['coils'] == 1) { ?>
							<?php echo htmlspecialchars($account->getUserPublicInfo($_GET['username'])['coils']); ?> Coil
						<?php } else { ?>
							<?php echo htmlspecialchars($account->getUserPublicInfo($_GET['username'])['coils']); ?> Coils
						<?php } ?>
					</div>
				<!-- Display if page does not belong to the user -->
				<?php } else { ?>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
						<?php echo htmlspecialchars($account->getUserPublicInfo($_GET['username'])['username']); ?>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
						<?php if ($account->getUserPublicInfo($_GET['username'])['coils'] == 0) { ?>
							<?php echo htmlspecialchars($account->getUserPublicInfo($_GET['username'])['coils']); ?> Coils
						<?php } else if ($account->getUserPublicInfo($_GET['username'])['coils'] == 1) { ?>
							<?php echo htmlspecialchars($account->getUserPublicInfo($_GET['username'])['coils']); ?> Coil
						<?php } else { ?>
							<?php echo htmlspecialchars($account->getUserPublicInfo($_GET['username'])['coils']); ?> Coils
						<?php } ?>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>

	<!-- Users Coils -->
	<div id="account_coils">
		<div class="container">
			<?php if (Account::listNumberOfCoils($_GET['username']) == 0) { ?>
				<h1><?php echo htmlspecialchars(Account::listNumberOfCoils($_GET['username'])); ?> Coils by <?php echo htmlspecialchars($_GET['username']); ?></h1>
			<?php } else if (Account::listNumberOfCoils($_GET['username']) == 1) { ?>
				<h1><?php echo htmlspecialchars(Account::listNumberOfCoils($_GET['username'])); ?> Coil by <?php echo htmlspecialchars($_GET['username']); ?></h1>
			<?php } else { ?>
				<h1><?php echo htmlspecialchars(Account::listNumberOfCoils($_GET['username'])); ?> Coils by <?php echo htmlspecialchars($_GET['username']); ?></h1>
			<?php } ?>
			<div class="row">
				<?php foreach (Account::listUsersCoils($_GET['username'], $_GET['page']) as $data) : ?>
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 coil">
						<a href="coil.php?coil_id=<?php echo htmlspecialchars($data['coil_id']); ?>&page=1">
							<img src="coils/<?php echo htmlspecialchars($data['coil_id']); ?>/coil_image.jpeg" class="img-responsive">
							<h3 class="text-center"><?php echo htmlspecialchars($data['coil_name']); ?></h3>
							<?php if ($data['views'] == 0) { ?>
								<p class="text-center"><?php echo htmlspecialchars(number_format($data['views'])); ?> Views</p>
							<?php } else if ($data['views'] == 1) {?>
								<p class="text-center"><?php echo htmlspecialchars(number_format($data['views'])); ?> View</p>
							<?php } else { ?>
								<p class="text-center"><?php echo htmlspecialchars(number_format($data['views'])); ?> Views</p>
							<?php } ?>
						</a>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>

	<!-- Users Pagination -->
	<div id="account_pagination" class="text-center">
		<div class="container">
			<!-- Verify there are comments -->
			<?php if (Account::listNumberOfCoils($_GET['username']) > 12) { ?>
				<div class="pagination">
					<!-- If page is eqaul to first don't display last button -->
					<?php if ($_GET['page'] > 1) { ?>
					<li><a href="account.php?username=<?php echo htmlspecialchars($_GET['username']); ?>&page=<?php echo htmlspecialchars($_GET['page']) - 1; ?>">Last</a></li>
					<?php } ?>
					<!-- List numerical pages -->
					<?php for($x = 1; $x <= Account::listUserPagination($_GET['username'], $_GET['page']); $x++) : ?>
						<li><a href="account.php?username=<?php echo htmlspecialchars($_GET['username']); ?>&page=<?php echo htmlspecialchars($x); ?>"<?php if($_GET['page'] == $x) {echo htmlspecialchars("class=selected"); }?>><?php echo htmlspecialchars($x); ?></a></li>
					<?php endfor; ?>
					<!-- If page is eqaul to last don't display next button -->
					<?php if ($_GET['page'] < Account::listUserPagination($_GET['username'], $_GET['page'])) { ?>
					<li><a href="account.php?username=<?php echo htmlspecialchars($_GET['username']); ?>&page=<?php echo htmlspecialchars($_GET['page']) + 1; ?>">Next</a></li>
					<?php } ?>
				</div>
			<?php } ?>
		</div>
	</div>

	<!-- Footer -->
	<?php require 'templates/footer.php' ?>
</body>
</html>