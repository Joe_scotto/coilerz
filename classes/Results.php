<?php

class Results {
	/**
	 * Returns the coils based on the search term
	 * @param  string $search_term What to search for
	 * @return array Coils found for that search term
	 */
	public static function listFoundCoils ($search_term, $page) {
		//Database Connection
		$db = Database::getInstance();

		//User Input
		$page = isset($page) ? (int)$page : 1;
		$perPage = 12;

		//Positioning
		$start = ($page > 1) ? ($page * $perPage) - $perPage : 0;

		//Select comments from database based on $coil_id
		$query = $db->getConnection()->prepare("SELECT * FROM `coils` WHERE coil_name LIKE :search_term OR wire LIKE :search_term OR username LIKE :search_term OR resistance LIKE :search_term OR atomizer LIKE :search_term OR difficulty LIKE :search_term ORDER BY id DESC LIMIT {$start}, {$perPage}");
		$query->execute(array(
			':search_term' => '%' . $search_term . '%'
		));

		//Return results
		return $query;
	}

	/**
	 * Pagination for search 
	 * @param  string $search_term What to search for
	 * @param  int $page Current page
	 * @return int Number of pages
	 */
	public static function listPagination ($search_term, $page) {
		//Database Connection
		$db = Database::getInstance();

		//User Input
		$page = isset($page) ? (int)$page : 1;
		$perPage = 12;

		//Set page to one if lower
		$start = ($page > 1) ? ($page * $perPage) - $perPage : 0;

		//Query to get total
		$totalQuery = $db->getConnection()->prepare("SELECT id FROM `coils` WHERE coil_name LIKE :search_term OR wire LIKE :search_term OR username LIKE :search_term OR resistance LIKE :search_term OR atomizer LIKE :search_term OR difficulty LIKE :search_term");
		$totalQuery->execute(array(
			':search_term' => '%' . $search_term . '%'
		));

		//Total number of pages
		$total = $totalQuery->rowCount();
		$pages = ceil($total / $perPage);

		return $pages;
	}

	/**
	 * Returns formatted number of coils
	 * @param  string $coil_id ID of the coil
	 * @return string Number of coils with proper grammar
	 */
	public static function getNumberOfCoils ($search_term) {
		//Database Connection
		$db = Database::getInstance();

		//Select all rows from database where $coil_id matches
		$query = $db->getConnection()->prepare("SELECT id FROM `coils` WHERE coil_name LIKE :search_term OR wire LIKE :search_term OR username LIKE :search_term OR resistance LIKE :search_term OR atomizer LIKE :search_term OR difficulty LIKE :search_term");
		$query->execute(array(
			':search_term' => '%' . $search_term . '%'
		));

		//Number of rows
		$rowCount = $query->rowCount();

		//Format based off data
		if ($rowCount == 0) {
			return '0 Results for "' . $search_term . '"'; 
		} else if ($rowCount == 1) {
			return '1 Result for "' . $search_term . '"';
		} else {
			return number_format($rowCount) . ' Results for "' . $search_term . '"';
		}
	}

	/**
	 * Returns number of found rows
	 * @param  string $search_term ID of the coil
	 * @return int Number of builds found
	 */
	public static function getNumericalNumberOfCoils ($search_term) {
		//Database Connection
		$db = Database::getInstance();

		//Select all rows from database where $coil_id matches
		$query = $db->getConnection()->prepare("SELECT id FROM `coils` WHERE coil_name LIKE :search_term OR wire LIKE :search_term OR username LIKE :search_term OR resistance LIKE :search_term OR atomizer LIKE :search_term OR difficulty LIKE :search_term");
		$query->execute(array(
			':search_term' => '%' . $search_term . '%'
		));

		//Number of rows
		return $query->rowCount();
	}
}