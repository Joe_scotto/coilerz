<?php

class Coil {
	public $page;

	/**
	 * Grabs coil information from the database based on an id
	 * @param  string $coil_id ID of the coil
	 * @return mixed Data found where ID matches
	 */
	public function getCoilInfo ($coil_id) {
		//Database Connection
		$db = Database::getInstance();

		//Query to select all information from `coils` tables based off $coil_id
		$query = $db->getConnection()->prepare("SELECT * FROM `coils` WHERE coil_id = :coil_id");
		$query->execute(array(
			':coil_id' => $coil_id
		));

		//Return array with results
		return $query->fetch(PDO::FETCH_ASSOC);
	}

	/**
	 * [updateViews description]
	 * @param  string $coil_id ID of the coil
	 * @param  string $ip Users IP address
	 * @return bool true if success false if fail
	 */
	public function updateViews ($coil_id, $ip) {
		//Database Connection
		$db = Database::getInstance();

		//Query to see if user view was found in table
		$query = $db->getConnection()->prepare("SELECT * FROM `views` WHERE ip = :ip AND coil_id = :coil_id");
		$query->execute(array(
			':ip' => $ip,
			':coil_id' => $coil_id
		));

		//Return rows found
		$rowCount = $query->rowCount();

		if ($rowCount == 0) {
			//Query to update `views` table with ip and coil_id
			$queryUpdate = $db->getConnection()->prepare("INSERT INTO `views` (ip, coil_id) VALUES (:ip, :coil_id)");
			$queryUpdate->execute(array(
				':ip' => $ip,
				':coil_id' => $coil_id
			));

			//Grab views from coils table
			$queryCoil = $db->getConnection()->prepare("SELECT views FROM `coils` WHERE coil_id = :coil_id");
			$queryCoil->execute(array(
				':coil_id' => $coil_id
			));

			//Grab and update views
			$views = $queryCoil->fetch(PDO::FETCH_ASSOC)['views'];
			$newViews = $views + 1;

			//Update views with new value
			$queryViews = $db->getConnection()->prepare("UPDATE `coils` SET views = :views WHERE coil_id = :coil_id");
			$queryViews->execute(array(
				':views' => $newViews,
				':coil_id' => $coil_id
			));
		} else if ($rowCount >= 1) {
			$queryUpdate = $db->getConnection()->prepare("UPDATE `views` SET visits = visits + 1 WHERE coil_id = :coil_id AND ip = :ip");
			$queryUpdate->execute(array(
				':coil_id' => $coil_id,
				':ip' => $ip
			));
		}
	}

	/**
	 * Adds a
	 * @param string $session_id Users session
	 * @param [type] $coil_id    [description]
	 * @return bool True if success, false if not
	 */
	public function addComment ($session_id, $coil_id, $data, $url) {
		//Database Connection
		$db = Database::getInstance();

		//Define query values
		$time = date("F j, Y", time());
		$username = Account::getUserPersonalInfo($session_id)['username'];
		$comment = $data['coil_comment_comment'];

		//Insert new comment
		$query = $db->getConnection()->prepare("INSERT INTO `comments` (coil_id, username, comment, date_added) VALUES (:coil_id, :username, :comment, :time)");
		$query->execute(array(
			':coil_id' => $coil_id,
			':username' => $username,
			':comment' => $comment,
			':time' => $time
		));

		//Redirects to prevent double submission
		header("Location: " . $url);
	}

	/**
	 * Returns formatted number of comments
	 * @param  string $coil_id ID of the coil
	 * @return string Number of coils with proper grammar
	 */
	public function getNumberOfComments ($coil_id) {
		//Database Connection
		$db = Database::getInstance();

		//Select all rows from database where $coil_id matches
		$query = $db->getConnection()->prepare("SELECT coil_id FROM `comments` WHERE coil_id = :coil_id");
		$query->execute(array(
			':coil_id' => $coil_id
		));

		$rowCount = $query->rowCount();

		if ($rowCount == 0) {
			return "0 Comments"; 
		} else if ($rowCount == 1) {
			return "1 Comment";
		} else {
			return $rowCount . " Comments";
		}
	}

	/**
	 * Checks if coil exists and redirects if it does not
	 * @param  string $coil_id ID of the coil
	 * @return bool True if exists, false if not
	 */
	public static function checkIfCoilsExists ($coil_id) {
		//Database Connection 
		$db = Database::getInstance();

		//Check if coil is found in database
		$query = $db->getConnection()->prepare("SELECT coil_id FROM `coils` WHERE coil_id = :coil_id");
		$query->execute(array(
			':coil_id' => $coil_id
		));

		//Verify coil exists and send 404 if not found
		if ($query->rowCount() <= 0) {
			header("Location: 404.php");
		} 
	}

	/**
	 * Returns the comments for the specified coil
	 * @param  string $coil_id ID of the coil
	 * @return array Comments found for that coil
	 */
	public static function listComments ($coil_id, $page) {
		//Database Connection
		$db = Database::getInstance();

		//User Input
		$page = isset($page) ? (int)$page : 1;
		$perPage = 10;

		//Positioning
		$start = ($page > 1) ? ($page * $perPage) - $perPage : 0;

		//Select comments from database based on $coil_id
		$query = $db->getConnection()->prepare("SELECT * FROM `comments` WHERE coil_id = :coil_id ORDER BY id DESC LIMIT {$start}, {$perPage}");
		$query->execute(array(
			':coil_id' => $coil_id
		));

		//Return results
		return $query;
	}

	/**
	 * Pagination for comments
	 * @param  string $coil_id ID of the coil
	 * @param  int $page Current page
	 * @return int Number of pages
	 */
	public static function commentPagination ($coil_id, $page) {
		//Database Connection
		$db = Database::getInstance();

		//User Input
		$page = isset($page) ? (int)$page : 1;
		$perPage = 10;

		//Set page to one if lower
		$start = ($page > 1) ? ($page * $perPage) - $perPage : 0;

		//Query to get total
		$totalQuery = $db->getConnection()->prepare("SELECT id FROM `comments` WHERE coil_id = :coil_id");
		$totalQuery->execute(array(
			':coil_id' => $coil_id
		));

		//Total number of pages
		$total = $totalQuery->rowCount();
		$pages = ceil($total / $perPage);

		return $pages;
	}

	/**
	 * Sends user to correct function based on if they have liked before or not
	 * @param  string $session_id Users ID
	 * @param  string $coil_id ID of the coil
	 * @param  string $ratingType Like or Dislike
	 * @param  string $url URL of the page like was sent from
	 * @return bool
	 */
	public function initiateRating ($session_id, $coil_id, $ratingType, $url) {
		//Database Connection
		$db = Database::getInstance();

		//Select all from ratings table
		$query = $db->getConnection()->prepare("SELECT * FROM `ratings` WHERE username = :username AND coil_id = :coil_id");
		$query->execute(array(
			':username' => Account::getUserPersonalInfo($session_id)['username'],
			':coil_id' => $coil_id
		));

		//Return rowcount and results
		$queryResults = array(
			'results' => $query->fetch(PDO::FETCH_ASSOC),
			'rowcount' => $query->rowCount()
		);

		//Check if user has rated the coil before
		if ($queryResults['rowcount'] == 0) {
			//Check if user liked or disliked
			if ($ratingType == "like") {
				$this->newLike($queryResults, $session_id, $coil_id, $url);
			} else if ($ratingType="dislike") {
				$this->newDislike($queryResults, $session_id, $coil_id, $url);
			}
		} else {
			//Check if user liked or disliked
			if ($ratingType == "like") {
				$this->updateLike($queryResults, $session_id, $coil_id, $url);
			} else if ($ratingType="dislike") {
				$this->updateDislike($queryResults, $session_id, $coil_id, $url);
			}
		}
	}

	/**
	 * Adds a new like if user does not exist with that coil_id in ratings database
	 * @param  string $results Sent from initiation query
	 * @param  string $session_id Users ID
	 * @param  string $coil_id ID of the coil
	 * @param  string $url URL of the page like was sent from
	 * @return bool
	 */
	private function newLike ($results, $session_id, $coil_id, $url) {
		//Database connection 
		$db = Database::getInstance();

		//Add user to ratings table with like
		$addUserToRatingsQuery = $db->getConnection()->prepare("INSERT INTO `ratings` (username, coil_id, liked) VALUES (:username, :coil_id, :liked)");
		$addUserToRatingsQuery->execute(array(
			':username' => Account::getUserPersonalInfo($session_id)['username'],
			':coil_id' => $coil_id,
			':liked' => 1
		));

		//Update coil likes
		$updateCoilLikes = $db->getConnection()->prepare("UPDATE `coils` SET likes = likes + 1 WHERE coil_id = :coil_id");
		$updateCoilLikes->execute(array(
			':coil_id' => $coil_id
		));

		//Redirect user to page
		header("Location:" . $url);
	}

	/**
	 * Adds a new dislike if user does not exist with that coil_id in ratings database
	 * @param  string $results Sent from initiation query
	 * @param  string $session_id Users ID
	 * @param  string $coil_id ID of the coil
	 * @param  string $url URL of the page like was sent from
	 * @return bool
	 */
	private function newDislike ($results, $session_id, $coil_id, $url) {
		//Database connection 
		$db = Database::getInstance();

		//Add user to ratings table with dislike
		$addUserToRatingsQuery = $db->getConnection()->prepare("INSERT INTO `ratings` (username, coil_id, disliked) VALUES (:username, :coil_id, :disliked)");
		$addUserToRatingsQuery->execute(array(
			':username' => Account::getUserPersonalInfo($session_id)['username'],
			':coil_id' => $coil_id,
			':disliked' => 1
		));

		//Update coil dislikes
		$updateCoilLikes = $db->getConnection()->prepare("UPDATE `coils` SET dislikes = dislikes + 1 WHERE coil_id = :coil_id");
		$updateCoilLikes->execute(array(
			':coil_id' => $coil_id
		));

		//Redirect user to page
		header("Location:" . $url);
	}

	/**
	 * Updates the like where the user exists in the database
	 * @param  string $results Sent from initiation query
	 * @param  string $session_id Users ID
	 * @param  string $coil_id ID of the coil
	 * @param  string $url URL of the page like was sent from
	 * @return bool
	 */
	private function updateLike ($results, $session_id, $coil_id, $url) {
		//Database connection 
		$db = Database::getInstance();

		if ($results['results']['liked'] == 0 && $results['results']['disliked'] == 0) {
			//Update user in ratings table
			$updateUserRatingQuery = $db->getConnection()->prepare("UPDATE `ratings` SET liked = :liked, disliked = :disliked WHERE username = :username AND coil_id = :coil_id");
			$updateUserRatingQuery->execute(array(
				':liked' => 1,
				':disliked' => 0,
				':username' => Account::getUserPersonalInfo($session_id)['username'],
				':coil_id' => $coil_id
			));

			//Update likes on coil table
			$updateCoilQuery = $db->getConnection()->prepare("UPDATE `coils` SET likes = likes + 1 WHERE coil_id = :coil_id");
			$updateCoilQuery->execute(array(
				':coil_id' => $coil_id
			));
		} else if ($results['results']['liked'] == 0 && $results['results']['disliked'] == 1) {
			//Update user in ratings table
			$updateUserRatingQuery = $db->getConnection()->prepare("UPDATE `ratings` SET liked = :liked, disliked = :disliked WHERE username = :username AND coil_id = :coil_id");
			$updateUserRatingQuery->execute(array(
				':liked' => 1,
				':disliked' => 0,
				':username' => Account::getUserPersonalInfo($session_id)['username'],
				':coil_id' => $coil_id
			));

			//Check if dislikes are greater than 0
			$currentDislikes = $this->getCoilInfo($coil_id)['dislikes'];

			if ($currentDislikes == 0) {
				$updatedDislikes = 0;
			} else {
				$updatedDislikes = $currentDislikes - 1;
			}

			//Update likes on coil table
			$updateCoilQuery = $db->getConnection()->prepare("UPDATE `coils` SET likes = likes + 1, dislikes = :dislikes WHERE coil_id = :coil_id");
			$updateCoilQuery->execute(array(
				':coil_id' => $coil_id,
				':dislikes' => $updatedDislikes
			));
		} else if ($results['results']['liked'] == 1 && $results['results']['disliked'] == 0) {
			//Update user in ratings table
			$updateUserRatingQuery = $db->getConnection()->prepare("UPDATE `ratings` SET liked = :liked WHERE username = :username AND coil_id = :coil_id");
			$updateUserRatingQuery->execute(array(
				':liked' => 0,
				':username' => Account::getUserPersonalInfo($session_id)['username'],
				':coil_id' => $coil_id
			));

			//Check if dislikes are greater than 0
			$currentLikes = $this->getCoilInfo($coil_id)['likes'];

			if ($currentLikes == 0) {
				$updatedLikes = 0;
			} else {
				$updatedLikes = $currentLikes - 1;
			}

			//Update likes on coil table
			$updateCoilQuery = $db->getConnection()->prepare("UPDATE `coils` SET likes = :likes WHERE coil_id = :coil_id");
			$updateCoilQuery->execute(array(
				':coil_id' => $coil_id,
				':likes' => $updatedLikes
			));
		}

		//Redirect user to page
		header("Location:" . $url);
		echo $url;
	}

	/**
	 * Updates the dislike where the user exists in the database
	 * @param  string $results Sent from initiation query
	 * @param  string $session_id Users ID
	 * @param  string $coil_id ID of the coil
	 * @param  string $url URL of the page like was sent from
	 * @return bool
	 */
	private function updateDislike ($results, $session_id, $coil_id, $url) {
		//Database connection 
		$db = Database::getInstance();

		if ($results['results']['disliked'] == 0 && $results['results']['liked'] == 0) {
			//Update user in ratings table
			$updateUserRatingQuery = $db->getConnection()->prepare("UPDATE `ratings` SET disliked = :disliked, liked = :liked WHERE username = :username AND coil_id = :coil_id");
			$updateUserRatingQuery->execute(array(
				':disliked' => 1,
				':liked' => 0,
				':username' => Account::getUserPersonalInfo($session_id)['username'],
				':coil_id' => $coil_id
			));

			//Update likes on coil table
			$updateCoilQuery = $db->getConnection()->prepare("UPDATE `coils` SET dislikes = dislikes + 1 WHERE coil_id = :coil_id");
			$updateCoilQuery->execute(array(
				':coil_id' => $coil_id
			));
		} else if ($results['results']['disliked'] == 0 && $results['results']['liked'] == 1) {
			//Update user in ratings table
			$updateUserRatingQuery = $db->getConnection()->prepare("UPDATE `ratings` SET disliked = :disliked, liked = :liked WHERE username = :username AND coil_id = :coil_id");
			$updateUserRatingQuery->execute(array(
				':disliked' => 1,
				':liked' => 0,
				':username' => Account::getUserPersonalInfo($session_id)['username'],
				':coil_id' => $coil_id
			));

			//Check if dislikes are greater than 0
			$currentLikes = $this->getCoilInfo($coil_id)['likes'];

			if ($currentLikes == 0) {
				$updatedLikes = 0;
			} else {
				$updatedLikes = $currentLikes - 1;
			}

			//Update likes on coil table
			$updateCoilQuery = $db->getConnection()->prepare("UPDATE `coils` SET dislikes = dislikes + 1, likes = :likes WHERE coil_id = :coil_id");
			$updateCoilQuery->execute(array(
				':coil_id' => $coil_id,
				':likes' => $updatedLikes
			));
		} else if ($results['results']['disliked'] == 1 && $results['results']['liked'] == 0) {
			//Update user in ratings table
			$updateUserRatingQuery = $db->getConnection()->prepare("UPDATE `ratings` SET disliked = :disliked WHERE username = :username AND coil_id = :coil_id");
			$updateUserRatingQuery->execute(array(
				':disliked' => 0,
				':username' => Account::getUserPersonalInfo($session_id)['username'],
				':coil_id' => $coil_id
			));

			//Check if dislikes are greater than 0
			$currentDislikes = $this->getCoilInfo($coil_id)['dislikes'];

			if ($currentDislikes == 0) {
				$updatedDislikes = 0;
			} else {
				$updatedDislikes = $currentDislikes - 1;
			}

			//Update likes on coil table
			$updateCoilQuery = $db->getConnection()->prepare("UPDATE `coils` SET dislikes = :dislikes WHERE coil_id = :coil_id");
			$updateCoilQuery->execute(array(
				':coil_id' => $coil_id,
				':dislikes' => $updatedDislikes
			));
		}

		//Redirect user to page
		header("Location: " . $url);
	}

	/**
	 * Changes color of like/dislike button based on what user put
	 * @param  string $session_id Session of the user
	 * @param  string $coil_id ID of the coil
	 * @return string Session with class name
	 */
	public static function checkIfLikedOrDisliked ($session_id, $coil_id) {
		//Database Connection
		$db = Database::getInstance();

		//Grab likes and dislikes from ratings table
		$query = $db->getConnection()->prepare("SELECT liked, disliked FROM `ratings` WHERE username = :username AND coil_id = :coil_id");
		$query->execute(array(
			':username' => Account::getUserPersonalInfo($session_id)['username'],
			':coil_id' => $coil_id
		));

		//Return Results
		$queryResults = $query->fetch(PDO::FETCH_ASSOC);

		if ($queryResults['liked'] == 1 && $queryResults['disliked'] == 0) {
			$returnLiked = "rated";
		} else if ($queryResults['disliked'] == 1 && $queryResults['liked'] == 0) {
			$returnDisliked = "rated";
		} else {
			$return = "";
		}

		return array(
			'like' => $returnLiked,
			'dislike' => $returnDisliked
		);
	}

	/**
	 * Checks if the coil belongs to a logged in user.
	 * @param  string $session_id Users ID
	 * @param  string $coil_id ID of the coil
	 * @return bool True if found, false if not.
	 */
	public static function checkCoilOwner ($session_id, $coil_id) {
		//Database Connection
		$db = Database::getInstance();

		//Check if user is found for that coil
		$query = $db->getConnection()->prepare("SELECT id FROM coils WHERE coil_id = :coil_id AND username = :username");
		$query->execute(array(
			':coil_id' => $coil_id,
			':username' => Account::getUserPersonalInfo($session_id)['username']
		));

		//Return rowcount
		$rowCount = $query->rowCount();

		if ($rowCount == 1) {
			return true;
		} else if ($rowCount < 1) {
			return false;
		}
	}

	/**
	 * Deletes a coil from Coilerz
	 * @param  string $session_id Users ID
	 * @param  string $coil_id ID of the coil
	 * @param  string $url Current page
	 * @return string Message of success or failure
	 */
	public function deleteCoil ($session_id, $coil_id, $url) {
		//Database Connection
		$db = Database::getInstance();

		//Validate coil belongs to current user. 
		if ($this->checkCoilOwner($session_id, $coil_id)) {
			//Delete coil from database
			$query = $db->getConnection()->prepare("DELETE FROM coils WHERE coil_id = :coil_id; 
												    DELETE FROM ratings WHERE coil_id = :coil_id; 
												    DELETE FROM comments WHERE coil_id = :coil_id; 
												    DELETE FROM views WHERE coil_id = :coil_id");
			$query->execute(array(
				':coil_id' => $coil_id
			));

			//Subtract one coil from user if not equal to 0
			$queryUser = $db->getConnection()->prepare("SELECT * FROM users WHERE session_id = :session_id");
			$queryUser->execute(array(
				':session_id' => $session_id
			));

			$results = $queryUser->fetch(PDO::FETCH_ASSOC);

			if ($results['coils'] > 0) {
				$queryUpdateCoils = $db->getConnection()->prepare("UPDATE users SET coils = coils - 1 WHERE session_id = :session_id");
				$queryUpdateCoils->execute(array(
					':session_id' => $session_id
				));
			} else if ($results['coils'] == 0) {
				return true;
			}

			//Delete files
			unlink("coils/" . $coil_id . "/coil_image.jpeg");
			rmdir("coils/" . $coil_id);

			//Redirect
			header("Location: index.php");
			exit();
		} else {
			//Set error message
			setcookie("sessionPersist", 1);
     		$_SESSION['message'] = "You don't own this coil.";

     		//Redirect
   			header("Location: " . $url);
   			exit();
		}
	}

	/**
	 * Starts the process of editing a coil on the database
	 * @param  string $session_id Users identifier
	 * @param  string $coil_id ID of the coil
	 * @param  string $url URL to post back to
	 * @return string Message if success or failure
	 */
	public function initiateEdit ($data, $session_id, $coil_id, $url) {
		//Database Connection
		$db = Database::getInstance();

		//Initiate functions 
		$wire = $this->formatWire($data);
		$wire_gauges = $this->formatWireGauges($data);
		$wire_types = $this->formatWireTypes($data);

		//Directory of coil
		$directory = 'coils/' . $coil_id;

		//Upload image
		$this->uploadImage($data, $directory);

		//Insert Data
		$query = $db->getConnection()->prepare("UPDATE `coils` SET coil_name = :coil_name, 
																 resistance = :resistance,
																 inner_diameter = :inner_diameter,
																 wraps = :wraps,
																 coils = :coils,
																 atomizer = :atomizer,
																 difficulty = :difficulty,
																 description = :description,
																 wire = :wire,
																 wire_gauges = :wire_gauges,
																 wire_types = :wire_types
																 WHERE coil_id = :coil_id AND username = :username");
		$query->execute(array(
			':coil_name' => $data['edit_coil_name'],
			':resistance' => $data['edit_resistance'],
			':inner_diameter' => $data['edit_inner_diameter'] + 0 . $data['edit_inner_diameter_unit'],
			':wraps' => $data['edit_wraps'] + 0,
			':coils' => $data['edit_number_of_coils'],
			':atomizer' => $data['edit_atomizer'],
			':difficulty' => $this->checkDifficulty($_POST),
			':description' => $data['edit_description'],
			':wire' => $wire,
			'wire_gauges' => $wire_gauges,
			'wire_types' => $wire_types,
			':coil_id' => $coil_id,
			':username' => Account::getUserPersonalInfo($session_id)['username']
		));

		//Return message to user notifying them of a successful upload
		$_SESSION['message'] = "Edit successful";
		setcookie("sessionPersist", 1);
		header("Location: " . $url);
	}

	/**
	 * Verifies that the user did not modify the value of difficulty option, sends formatted version
	 * @param  mixed $data $_POST Data
	 * @return string Formatted version of difficulty
	 */
	public static function checkDifficulty ($data) {
		switch($data['edit_difficulty']) {
			case "easy" : $return = "Easy"; 
			break;

			case "medium" : $return = "Medium";
			break;

			case "hard" : $return = "Hard";
			break;

			case "experimental" : $return = "Experimental";
			break;

			default : $return = "Error";
		}

		return $return;
	}

	/**
	 * Verifies that the user did not modify the value of inner diameter option, sends formatted version
	 * @param  mixed $data $_POST Data
	 * @return string Formatted version of difficulty
	 */
	public static function checkInnerDiameter ($data) {
		switch($data['edit_inner_diameter_unit']) {
			case "mm" : $return = "mm"; 
			break;

			case "in" : $return = "in";
			break;

			default : $return = "Error";
		}

		return $return;
	}

	/**
	 * Verifies that the user did not modify the value of wire option, sends formatted version
	 * @param  mixed $data $_POST Data
	 * @return string Formatted version of difficulty
	 */
	public static function checkWireType ($data) {
		switch($data) {
			case "Kanthal" : $return = "Kanthal"; 
			break;

			case "N200" : $return = "N200";
			break;

			case "N80" : $return = "N80";
			break;

			case "Stainless Steel" : $return = "Stainless Steel";
			break;

			case "Titanium" : $return = "Titanium";
			break;

			default : $return = "Error";
		}

		return $return;
	}

	/**
	 * Prepares the raw data for the database.
	 * @param  mixed $data $_POST data
	 * @return string Formatted wires ready for the database
	 */
	public function formatWire ($data) {
		//Wire One
		if (!empty($data['edit_wire_one_gauge']) && !empty($data['edit_wire_one_type'])) {
				$wire = $data['edit_wire_one_gauge'] . "g " . $data['edit_wire_one_type'];
		}

		//Wire Two
		if (!empty($data['edit_wire_two_gauge']) && !empty($data['edit_wire_two_type'])) {
				$wire .= " - " . $data['edit_wire_two_gauge'] . "g " . $data['edit_wire_two_type'];
		}

		//Wire Three
		if (!empty($data['edit_wire_three_gauge']) && !empty($data['edit_wire_three_type'])) {
				$wire .= " - " . $data['edit_wire_three_gauge'] . "g " . $data['edit_wire_three_type'];
		}

		//Wire Four
		if (!empty($data['edit_wire_four_gauge']) && !empty($data['edit_wire_four_type'])) {
				$wire .= " - " . $data['edit_wire_four_gauge'] . "g " . $data['edit_wire_four_type'];
		}

		//Wire Five
		if (!empty($data['edit_wire_five_gauge']) && !empty($data['edit_wire_five_type'])) {
				$wire .= " - " . $data['edit_wire_five_gauge'] . "g " . $data['edit_wire_five_type'];
		}

		//Wire Six		
		if (!empty($data['edit_wire_six_gauge']) && !empty($data['edit_wire_six_type'])) {
				$wire .= " - " . $data['edit_wire_six_gauge'] . "g " . $data['edit_wire_six_type'];
		}

		//Returns string with first letter in each word uppercase
		return ucwords($wire);
	}

	/**
	 * Prepares the raw data for the database.
	 * @param  mixed $data $_POST data
	 * @return string Formatted wire gauges ready for the database
	 */
	public function formatWireGauges ($data) {
		//Wire One
		if (!empty($data['edit_wire_one_gauge']) && !empty($data['edit_wire_one_type'])) {
				$wire = $data['edit_wire_one_gauge'];
		}

		//Wire Two
		if (!empty($data['edit_wire_two_gauge']) && !empty($data['edit_wire_two_type'])) {
				$wire .= "," . $data['edit_wire_two_gauge'];
		}

		//Wire Three
		if (!empty($data['edit_wire_three_gauge']) && !empty($data['edit_wire_three_type'])) {
				$wire .= "," . $data['edit_wire_three_gauge'];
		}

		//Wire Four
		if (!empty($data['edit_wire_four_gauge']) && !empty($data['edit_wire_four_type'])) {
				$wire .= "," . $data['edit_wire_four_gauge'];
		}

		//Wire Five
		if (!empty($data['edit_wire_five_gauge']) && !empty($data['edit_wire_five_type'])) {
				$wire .= "," . $data['edit_wire_five_gauge'];
		}

		//Wire Six		
		if (!empty($data['edit_wire_six_gauge']) && !empty($data['edit_wire_six_type'])) {
				$wire .= "," . $data['edit_wire_six_gauge'];
		}

		//Returns string with first letter in each word uppercase
		return ucwords($wire);
	}

	/**
	 * Prepares the raw data for the database.
	 * @param  mixed $data $_POST data
	 * @return string Formatted wire types ready for the database
	 */
	public function formatWireTypes ($data) {
		//Wire One
		if (!empty($data['edit_wire_one_gauge']) && !empty($data['edit_wire_one_type'])) {
				$wire = $data['edit_wire_one_type'];
		}

		//Wire Two
		if (!empty($data['edit_wire_two_gauge']) && !empty($data['edit_wire_two_type'])) {
				$wire .= "," . $data['edit_wire_two_type'];
		}

		//Wire Three
		if (!empty($data['edit_wire_three_gauge']) && !empty($data['edit_wire_three_type'])) {
				$wire .= "," . $data['edit_wire_three_type'];
		}

		//Wire Four
		if (!empty($data['edit_wire_four_gauge']) && !empty($data['edit_wire_four_type'])) {
				$wire .= "," . $data['edit_wire_four_type'];
		}

		//Wire Five
		if (!empty($data['edit_wire_five_gauge']) && !empty($data['edit_wire_five_type'])) {
				$wire .= "," . $data['edit_wire_five_type'];
		}

		//Wire Six		
		if (!empty($data['edit_wire_six_gauge']) && !empty($data['edit_wire_six_type'])) {
				$wire .= "," . $data['edit_wire_six_type'];
		}

		//Returns string with first letter in each word uppercase
		return ucwords($wire);
	}

	/**
	 * Moves image to coils folder
	 * @param  string $session_id Session identifier for logged in user
	 * @param  mixed $data $_POST data
	 * @return bool True is image is uploaded, false if not.
	 */
	public function uploadImage ($data, $directory) {
		//Database Connection 
		$db = Database::getInstance();

		//Define variables
		$base64 = $data['edit_image'];
		unlink($directory . '/coil_image.jpeg');
		$directory = $directory . '/coil_image.jpeg';

		//Decode base64 into image
		$db->decodeBase64($base64, $directory);
	}
}
